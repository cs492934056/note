# JavaScript 对象



## 对象基础

对象是一个包含相关数据和方法的集合（通常由一些变量和函数组成，我们称之为对象里面的属性和方法），让我们通过一个例子来了解它们。

```js
var person = {};
```

如果你在浏览器控制台输入person，然后按下Enter(确认)键，你会得到如下结果：

```js
[object Object]
```





## 请不要把字符串、数值和布尔值声明为对象！

如果通过关键词 "new" 来声明 JavaScript 变量，则该变量会被创建为对象：

```
var x = new String();        // 把 x 声明为 String 对象
var y = new Number();        // 把 y 声明为 Number 对象
var z = new Boolean();       //	把 z 声明为 Boolean 对象
```

请避免字符串、数值或逻辑对象。他们会增加代码的复杂性并降低执行速度。



## 对象的直接赋值

对象类型的复制是引用,两个对象指向同一个指针,改变其中一个,会影响另一个的值.
所以这里需要克隆,而不是赋值.

```
var page = {
    a: 1
};
var page2 = page;
page2.a = 10;
console.log(page);  // {a: 10}

var page3 = Object.assign({}, page);
page3.a = 100;
console.log(page);  // {a: 10}
```

Object.assign是浅拷贝,对嵌套属性也是引用,如果有嵌套会出现问题,JSON.parse()可以实现深拷贝,但是如果对象属性为function,因为JSON格式字符串不支持function,在构建的时候会自动删除,所以可以写一个自己的深拷贝函数或者用一些库的深拷贝方法.





深拷贝和浅拷贝最根本的区别在于是否是真正获取了一个对象的复制实体，而不是引用，
 **深拷贝在计算机中开辟了一块内存地址用于存放复制的对象，而浅拷贝仅仅是指向被拷贝的内存地址，如果原地址中对象被改变了，那么浅拷贝出来的对象也会相应改变。**

## 1.深拷贝

1.1.  最简单的方法就是JSON.parse(JSON.stringify（））



```jsx
function deepCopy(o) {
    return JSON.parse(JSON.stringify(o))
}

var c = {
    age: 1,
    name: undefined,
    sex: null,
    tel: /^1[34578]\d{9}$/,
    say: () => {
        console.log('hahha')
    }
}
// { age: 1, sex: null, tel: {} }
```

**需要注意的是：这种拷贝方法不可以拷贝一些特殊的属性（例如正则表达式，undefine，function）**

1.2. 用递归去复制所有层级属性



```jsx
function deepCopyTwo(obj) {
    let objClone = Array.isArray(obj) ? [] : {};
    if (obj && typeof obj == 'object') {
        for (const key in obj) {
            //判断obj子元素是否为对象，如果是，递归复制
            if (obj[key] && typeof obj[key] === "object") {
                objClone[key] = deepCopyTwo(obj[key]);
            } else {
                //如果不是，简单复制
                objClone[key] = obj[key];
            }
        }
    }
    return objClone;
}
```

## 2. 浅拷贝

2.1. object.assign(target,source)

> 仅拷贝可枚举属性

 **Object.assign** 方法只复制源对象中可枚举的属性和对象自身的属性
 如果目标对象中的属性具有相同的键，则属性将被源中的属性覆盖。后来的源的属性将类似地覆盖早先的属性
 `Object.assign` 会跳过那些值为 [`null`] null 是一个 JavaScript 字面量，表示空值（null or an "empty" value），即没有对象被呈现（no object value is present）。它是 JavaScript 原始值 之一。") 或 [`undefined`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/undefined) 的源对象。   

 let tempBody = Object.assign({}, dbBody);

不能拷贝数组，会变成对象

第一个参数改成【】即可

 let tempBody = Object.assign([], dbBody);



2.2object.create



## 数组对象

```
var a = [], b = [];
console.log(a==b);
12
```

**控制台的打印结果是什么？答案是：false。
　　接下来看解析：
　　原始值的比较是值的比较：
　　　　它们的值相等时它们就相等（）
　　　　它们的值和类型都相等时它们就恒等（=）。**

**对象和原始值不同，对象的比较并非值的比较,而是引用的比较：
　　　　即使两个对象包含同样的属性及相同的值，它们也是不相等的
　　　　即使两个数组各个索引元素完全相等，它们也是不相等的**





## null、undefined、boolean

由于历史原因，JavaScript第一版中，数据类型没有null，null被归纳为对象

typeof null; // object

但是设计者觉得null 不能来表示“无”的值，所以设计出了undefined 

null 转化为数值时候，表示0

undefined 转化为数值时候，表示NaN

```
var a = null;
a+1; //1

var b = undefined;
b+1;  //NaN
```

null == undefined;  //true



如果JavaScript预期一个boolean类型的值，会将该值转化为boolean类型。转换除了以下的值，均为“true”，如下为“false”：

- undefined
- null
- 0
- ""或者''
- false
- NaN  (not a number)   与任何数值比较都返回 false

> 空数组 [] 以及 空对象 {} 均为true  
>
> NaN   不是数据类型，而是一个特殊数值，不等于任何值，包括他本身
>
> 由于数值正向溢出（overflow）、负向溢出（underflow）和被`0`除，JavaScript 都不报错，所以单纯的数学运算几乎没有可能抛出错误。



```
undefined==undefined
// true

null==null
// true

NaN==NaN
// false

typeof NaN
//" number"

0 / 0 
// NaN

1 / -0 
// -Infinity

-1 / -0 
// Infinity

Infinity - Infinity 
// NaN

Infinity / Infinity 
// NaN

0 * Infinity 
// NaN
```





## parseInt（param1,param2）的用法

1. 先将参数param1 去空格操作
2. 将参数param1转化为字符串格式
3. 逐个字符解析，知道遇到不能解析的字符（数值以及正负号除外）
4. 根据param2确定参数是什么几进制数，并返回正确的十进制数值，错误返回NaN

```
parseInt('10', 37) // NaN        2到36 之间有效
parseInt('10', 1) // NaN
parseInt('10', 0) // 10          忽略
parseInt('10', null) // 10		 忽略
parseInt('10', undefined) // 10  忽略

==========  0x11 => String(0x11) => "17" =============== 
parseInt(0x11, 36) // 43
parseInt(0x11, 2) // 1

// 等同于
parseInt(String(0x11), 36)
parseInt(String(0x11), 2)

// 等同于
    parseInt('17', 36)
parseInt('17', 2)
```



对于空数组和只有一个数值成员的数组，`isNaN`返回`false`。

上面代码之所以返回`false`，原因是这些数组能被`Number`函数转成数值，请参见《数据类型转换》一章。

```
isNaN([]) // false
isNaN([123]) // false
isNaN(['123']) // false
```

判断`NaN`更可靠的方法是，利用`NaN`为唯一不等于自身的值的这个特点，进行判断。

```js
function myIsNaN(value) {
  return typeof value === 'number' && isNaN(value);
}

function myIsNaN(value) {
  return value !== value;
}
```



```js
var a={a:1,b:2,c:{d:3}};
### 获取对象的所有可枚举属性
Object.keys(a);   //  ["a", "b", "c"]

### 判断是否存在属性,继承也是返回 true
'b' in a;  // true

### 
var obj={};
obj.hasOwnProperty('toString'); // false

### for...in 遍历属性
var obj = {a: 1, b: 2, c: 3};
for (var i in obj) {
  console.log('键名：', i);
  console.log('键值：', obj[i]);
}
// 键名： a
// 键值： 1
// 键名： b
// 键值： 2
// 键名： c
// 键值： 3
```

