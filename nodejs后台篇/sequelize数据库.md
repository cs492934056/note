# Squelize

### 简介

Sequelize是一个基于承诺的Node.js ORM，适用于Postgres、MySQL、MariaDB、SQLite和Microsoft SQL Server。它具有可靠的事务支持、关系、急于加载和延迟加载、读取复制等特性。

https://sequelize.org/master/manual/migrations.html



### 安装

```javascript
npm install --save sequelize

# One of the following:
$ npm install --save pg pg-hstore # Postgres
$ npm install --save mysql2       # 这里我们用mysql作为我们的数据库
$ npm install --save mariadb
$ npm install --save sqlite3
$ npm install --save tedious # Microsoft SQL Server

# 安装手脚架
npm install --save-dev sequelize-cli

# 搭建手脚架
sequelize init
# 或者
npx sequelize init
# 根据 zkat/npx 的描述，npx 会帮你执行依赖包里的二进制文件。本机有问题，直接运行上条指令了~
```

运行后会添加如下文件夹：

![image-20201109170818433](Untitled.assets/image-20201109170818433.png)

![image-20201109170823662](Untitled.assets/image-20201109170823662.png)

- `config`, 它告诉CLI如何与数据库连接；

- `models`, 包含项目的所有模型；

  > 模型是Sequelize的本质。模型是表示数据库中表的抽象。在Sequelize中，它是一个扩展模型的类。
  >
  > 这个模型告诉Sequelize关于它所表示的实体的一些事情，比如数据库中的表的名称以及它拥有哪些列(以及它们的数据类型)。
  >
  > Sequelize中的模型有一个名称。此名称不必与它在数据库中表示的表的名称相同。通常，模型有单数名称(如User)，而表有复数名称(如User)，尽管这是完全可配置的。

- `migrations`, 包含所有迁移文件；

  > 就像使用版本控制系统(如Git)来管理源代码中的更改一样，也可以使用迁移来跟踪数据库的更改。通过迁移，您可以将现有数据库转换为另一种状态，反之亦然:这些状态转换保存在迁移文件中，这些文件描述了如何到达新状态，以及如何恢复更改以返回到旧状态。
  >
  > 您将需要Sequelize命令行接口(CLI)。CLI提供了对迁移和项目启动的支持。
  >
  > Sequelize中的迁移是一个javascript文件，它导出两个函数，向上和向下，这两个函数指示如何执行迁移和撤销迁移。你手动定义这些函数，但你不会手动调用它们;它们将被CLI自动调用。在这些函数中，您只需在sequelize的帮助下执行所需的任何查询。查询和Sequelize提供的任何其他方法。除此之外没有其他的魔法。

- `seeders`, 包含所有种子文件。

  > 假设我们想在默认情况下将一些数据插入到几个表中。如果我们继续前面的示例，我们可以考虑为user表创建一个演示用户。
  >
  > 要管理所有数据迁移，可以使用seeders。种子文件是一些数据更改，可用于用样例数据或测试数据填充数据库表。



### 配置数据库

通过生成的文件 **config/config.js** 可以配置数据库

```javascript
# 分别为开发，测试，生产环境
# 此配置是为了下面测试修改好的配置~
{
  "development": {
    "username": "suiyan",
    "password": "123456",
    "database": "yueyueniao",
    "host": "192.168.1.250",
    "dialect": "mysql"
  },
  "test": {
    "username": "suiyan",
    "password": "123456",
    "database": "yueyueniao",
    "host": "192.168.1.250",
    "dialect": "mysql"
  },
  "production": {
    "username": "suiyan",
    "password": "123456",
    "database": "yueyueniao",
    "host": "192.168.1.250",
    "dialect": "mysql"
  }
}
```



### 新建删除数据库

- ```javascript
  # 生成数据库
  sequelize db:create
  # 销毁数据库(yueyueniao为数据库名)
  sequelize db:drop yueyueniao
  ```

  ![image-20201110105622558](数据库.assets/image-20201110105622558.png)



### 新建数据库表

```javascript
#### 创建modal以及migrations
npx sequelize-cli model:generate --name User --attributes firstName:string,lastName:string,email:string
# 1.在模型文件夹中创建一个模型文件用户;
# 2.在migrations文件夹中创建一个名为xxxxxxxxxxxx - Create -user.js的迁移文件。
# 注释：Sequelize将只使用模型文件，它是表表示。另一方面，迁移文件是CLI使用的模型或更具体地说表中的更改。将迁移视为数据库中某些更改的提交或日志。

#### 运行migrations
sequelize-cli db:migrate
# 查看状态
sequelize db:migrate:status
```

新建迁移文件

![image-20201110103754092](数据库.assets/image-20201110103754092.png)

新建完迁移文件是不会操作数据库的，这个时候看看状态

![image-20201110112004608](数据库.assets/image-20201110112004608.png)

运行迁移文件

![image-20201110103715294](数据库.assets/image-20201110103715294.png)

查看运行迁移文件后的状态

![image-20201110112512912](数据库.assets/image-20201110112512912.png)

成功生成数据库表

![image-20201110103828049](数据库.assets/image-20201110103828049.png)



***\*重要：如果当前文件未被执行过状态为down ，执行过则为 up。\****



### 修改数据库表

```javascript
# 不用之前的指令，因为已经有对应的表跟modal，只需要简单的migration文件
sequelize migration:create --name addTest
```



![image-20201110140402550](数据库.assets/image-20201110140402550.png)

![image-20201110140447408](数据库.assets/image-20201110140447408.png)

修改如下：

```javascript
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
      await queryInterface.addColumn('users', 'test', Sequelize.STRING);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.removeColumn('user', 'test', Sequelize.STRING);
  }
};

## 具体可以去查看官方文档
https://sequelize.org/master/manual/query-interface.html
```

运行迁移文件

![image-20201110141355320](数据库.assets/image-20201110141355320.png)

查看数据库表，已经成功添加**test**列

![image-20201110141410226](数据库.assets/image-20201110141410226.png)



### 添加表数据

```javascript
# 创建种子文件
sequelize-cli seed:generate --name demo-user
# 编辑完后运行指令
sequelize-cli db:seed:all
```

![image-20201110144033186](数据库.assets/image-20201110144033186.png)

![image-20201110144019042](数据库.assets/image-20201110144019042.png)

打开文件并且编辑

```javascript
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
	await queryInterface.bulkInsert('Users', [{
       firstName: 'John',
       lastName: 'Doe',
	   createdAt:new Date(),
	   updatedAt:new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
	 await queryInterface.bulkDelete('Users', null, {});
  }
};
```

![image-20201111085042543](数据库.assets/image-20201111085042543.png)

![image-20201111091842252](数据库.assets/image-20201111091842252.png)