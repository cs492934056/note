### 一、配置基本环境

根据node环境手册

1）安装nvm

2)   安装nrm



### 二、上传代码

通过xftp等文件传输工具，将nodejs代码传到服务器



### 三、运行代码

```javascript
# 安装依赖包
npm i  
# 跑程序
npm run dev
```

![img](https://segmentfault.com/img/bV9oVl?w=1314&h=600)

出现类似的权限问题，chmod 777  ./node_modules/.bin/* 修改一下权限问题即可。



### 四、验证服务器环境

- 因为我的nodejs是用9500 端口，所以需要测试是否能够直接调用。

- 正常是不可以的，需要配置阿里云，这次直接弄了一个安全组，并将服务器加入其安全组。

![image-20201010221136665](nodeJs服务器配置.assets/image-20201010221136665.png)

![image-20201010220956582](nodeJs服务器配置.assets/image-20201010220956582.png)

- 认证端口开放可以通过 CMD 中 telnet 192.168.1.1 9500 ，携带的参数1是IP，参数2是端口号。
- POSTMAN直接调用接口试试，没问题的话就说明基本环境没问题。
- 测试没问题可以关闭阿里云安全组9500端口，因为网页请求一般是用nginx转发到9500即可，暴露端口越多越危险！



### 五、配置pm2

```javascript
# 杀死所有node进程
pkill node
# 安装
npm install pm2@latest -g
# 生成配置文件，ecosystem.config.js
pm2 ecosystem

# 修改配置文件
module.exports = {
  apps : [{
    name: "penk-node",
    script: "./penk-node-master/bin/www",
    instances:1,
    watch:false,
    max_memory_restart:'2G',
    env: {
      NODE_ENV: "development",
    },
    env_production: {
      NODE_ENV: "production",
    }
  }]
};

# 启动pm2通过配置文件的方式
pm2 start ecosystem.config.js
# 开机自启动
pm2 startup
pm2 save
```



### 六、自动化部署

> 之前的步骤一步不能少，验证了以上服务没问题才可进行一下操作
>
> 1. nodejs 的安装
> 2. 上传并且npm install 
> 3. npm run dev可运行
> 4. PM2配置后可运行

##### 配置GIT

```javascript
# 按照 git
yum –y install git
# 生产SSH密钥
ssh-keygen -t rsa -C "xxxxx@xxxxx.com"  

# 按照提示完成三次回车，即可生成 ssh key。通过查看 `~/.ssh/id_rsa.pub` 文件内容，获取到你的 public key
cat ~/.ssh/id_rsa.pub
# ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6eNtGpNGwstc....
```

> 注意：这里的 `xxxxx@xxxxx.com` 只是生成的 sshkey 的名称，并不约束或要求具体命名为某个邮箱。
> 现网的大部分教程均讲解的使用邮箱生成，其一开始的初衷仅仅是为了便于辨识所以使用了邮箱。

将密钥在github或者gitee中生成即可...



##### 配置PM2

> 这里我将前后端代码整合在一起打包，都在yyn这个文件中，前后端在一个git项目

```javascript
module.exports = {
  apps: [{
    name: "penk-node",
    script: "/penk/current/penk-node-master/bin/www",
    instances: 1,
    watch: false,
    max_memory_restart: '2G',
    env: {
      NODE_ENV: "development",
    },
    env_production: {
      NODE_ENV: "production",
    }
  }],
  deploy: {
    production: {
      user: "root",
      host: "120.77.204.132",
      ref: "origin/master",
      repo: "git@gitee.com:penk666/yyn.git",
      path: "/penk",
      "post-deploy": "git pull origin master && cd /penk/current/penk-node-master && npm install && pm2 startOrRestart /penk/current/ecosystem.config.js --env production"
    }
  }
};
```

执行命令，需要输入的密码，是服务器的密码，git已经设置了SSH公钥，无需密码~

```javascript
# 在window系统会有问题，需要在git中进行
pm2 deploy ecosystem.config.js production setup
# 之后有什么文件需要修改
pm2 deploy ecosystem.config.js production update
```

![image-20201015150756622](nodeJs服务器配置.assets/image-20201015150756622.png)

![image-20201015153412755](nodeJs服务器配置.assets/image-20201015153412755.png)



查看服务器中文件情况

![image-20201015152919494](nodeJs服务器配置.assets/image-20201015152919494.png)

![image-20201015153801179](nodeJs服务器配置.assets/image-20201015153801179.png)



> current 是运行程序的目录
>
> shared 存放logs等文件
>
> source 存放源码



### 总结

> PM2只要配置了配置文件，就可以通过git 更新代码并且运行的脚本，实现了自动化部署。