# nodejs 开发环境配置



nodejs 后台开发环境配置篇，需要配置node环境。我们可以如下操作：

1. nvm来管理node环境，兼容不同程序不同node版本。
2. nrm来管理源配置（一般npm都是直接用国外的，国内默认用taobao）。
3. 使用nodemon来配置热启动，方便调试。





## 一、nvm

### 1）简介

node version manager  node版本管理工具

### 2)  安装

1. 下载并安装在不带空格和中文的路径

   ```
   https://github.com/coreybutler/nvm-windows/releases
   ```

2. 在安装目录下配置settings.txt

   ```
   node_mirror: https://npm.taobao.org/mirrors/node/
   npm_mirror: https://npm.taobao.org/mirrors/npm/
   ```

   

### 3）常用指令

 1. 查看nvm版本。

    ```
    nvm -v
    ```

2. 帮助手册。

   ```
   nvm -h
   ```

 3. 查看当前系统存在的nodejs版本。

    ```
    nvm ls
    ```

 4. 查看远程源存在的版本。

    ```
    nvm ls available
    ```

 5. 安装指定版本，接版本，以及系统位数。

    ```
    nvm install 12.14.0 64
    ```

 6. 使用版本

    ```
    nvm use 12.14.0
    ```

    ![image-20191229160810950](node 环境手册.assets/image-20191229160810950.png)

![image-20191229161036440](node 环境手册.assets/image-20191229161036440.png)



## 二、nodemon

### 1）简介

> ​	nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.
>
> ​	nodemon does **not** require *any* additional changes to your code or method of development. nodemon is a replacement wrapper for `node`, to use `nodemon` replace the word `node` on the command line when executing your script.
>
> ​	nodemon不需要对您的代码或开发方法进行任何额外的更改。nodemon是node的替换包装器，在执行脚本时使用nodemon替换命令行上的node命令。
>



### 2）配置nodemon

1.  npm install nodemon -D

2. 修改package.json 中的启动命令

3. 通过nodemon.json 配置指定特殊的watch文件

4. 配置DEBUG模式

5. 帮助文档 nodemon -h

   ```
     "scripts": {
       "start:node": "node ./bin/www",
       "start": "SET DEBUG=express:* & nodemon ./bin/www",
       "serve": "SET DEBUG=express:* & npm start"
     }
   ```



## 三、nrm

### 1）简介

nrm  (npm registry manager ) 是一个 npm 源管理器，允许你快速地在 npm源间切换。

什么意思呢，npm默认情况下是使用npm官方源（使用npm config ls命令可以查看），在国内用这个源肯定是不靠谱的，一般我们都会用淘宝npm源：https://registry.npm.taobao.org/，修改源的方式也很简单，在终端输入：

```
npm set registry https://registry.npm.taobao.org/
```

再npm config ls查看，已经切换成功。

那么，问题来了，如果哪天你又跑去国外了，淘宝源肯定是用不了的，又要切换回官网源，或者哪天你们公司有自己的私有npm源了，又需要切换成公司的源，这样岂不很麻烦？于是有了[nrm](https://github.com/Pana/nrm)。

### 2）安装

```
npm install -g nrm
```

### 3）使用

##### 1. 查看源 

```
nrm ls
```

![image-20191229143506267](node 环境手册.assets/image-20191229143506267.png)

##### 2. 切换源

```
nrm use <registry>
```

![image-20191229143559116](node 环境手册.assets/image-20191229143559116.png)

##### 3. 测试源速度

```
nrm test <registry>
```

![image-20191229143725790](node 环境手册.assets/image-20191229143725790.png)



