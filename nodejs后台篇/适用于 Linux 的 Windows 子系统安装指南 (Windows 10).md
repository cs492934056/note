# 适用于 Linux 的 Windows 子系统安装指南 (Windows 10)



### 1、[官方安装](https://docs.microsoft.com/zh-cn/windows/wsl/install-win10)



### 2、运行报错

按照步骤1操作，报错（WSL安装Linux报错WslRegisterDistribution failed with error: 0x80370102）

原因：没有开启**虚拟化**。

需要在根据硬件主板进入BIOS，开启虚拟化。



> 这里说明以下为什么先安装WSL1再转换成WSL2啊，为什么不直接设置成 wsl --set-default-version 2 默认安装WSL2呢？
>
> 答案是会遇到报错： WslRegisterDistribution failed with error: 0x80370102 这个错误查了很多文档都解决不了，可是先安装WSL1再转换2就不会遇到这个报错。



### 3、设置管理员root

```sh
Windows PowerShell
版权所有 (C) Microsoft Corporation。保留所有权利。

尝试新的跨平台 PowerShell https://aka.ms/pscore6

PS C:\Users\Admin> wslconfig /l
适用于 Linux 的 Windows 子系统分发版:
Ubuntu (默认)
kali-linux
PS C:\Users\Admin> ubuntu config --default-user root
PS C:\Users\Admin> kali config --default-user root

### 根据提示设置好密码即可。
```

