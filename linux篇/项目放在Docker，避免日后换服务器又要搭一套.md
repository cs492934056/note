# 项目放在Docker，避免日后换服务器又要搭一套

[TOC]

> 宿主机：阿里云服务器
>
> docker1：宿主机安装的docker
>
> docker2：docker1安装的docker



```
# 默认情况下，在第一步执行的是 /bin/bash，而因为docker中的bug，无法使用systemctl，就无法启动docker2
# 所以我们使用了 /usr/sbin/init 同时 --privileged 这样就能够使用systemctl了，但覆盖了默认的 /bin/bash
# 因此我们如果想进入容器 就不能再使用 docker attach myCentos
# 而只能使用 docker exec -it web_project /bin/bash 因为 exec 可以让我们执行被覆盖掉的默认命令 /bin/bash
# 同时 -it 也是必须的。

docker run -itd  --privileged --name web_project centos /usr/sbin/init
docker exec -it web_project /bin/bash
```

![image-20220507174049903](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20220507174049903.png)



![image-20220507174029409](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20220507174029409.png)



![image-20220507173938255](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20220507173938255.png)





![image-20220507140025030](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20220507140025030.png)

> docker exec -it web_project /bin/bash



![image-20220507152023778](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20220507152023778.png)

> 安装失败
>
> 原因
> 在2022年1月31日，CentOS团队终于从官方镜像中移除CentOS 8的所有包。
> CentOS 8已于2021年12月31日寿终正非，但软件包仍在官方镜像上保留了一段时间。现在他们被转移到https://vault.centos.org



![image-20220507152219971](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20220507152219971.png)

>解决方法
>如果你仍然需要运行CentOS 8，你可以在/etc/yum.repos.d中更新一下源。使用vault.centos.org代替mirror.centos.org。
>
>sed -i -e "s|mirrorlist=|#mirrorlist=|g" /etc/yum.repos.d/CentOS-*
>sed -i -e "s|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g" /etc/yum.repos.d/CentOS-*



### 1）Docker 安装 Docker

> 一键安装安装命令如下：
>
> ```
> curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
> systemctl start docker
> ```

![image-20220507162633867](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20220507162633867.png)





安装JENKINS

https://www.cnblogs.com/ming-blogs/p/10903408.html

docker exec -it 7f485bd95c3b  /bin/bash



安装 Portainer

https://blog.csdn.net/woniu211111/article/details/108675525



node项目部署

http://www.360doc.com/content/20/1008/15/61746833_939429398.shtml



JENKINS+NODES

https://wenku.baidu.com/view/5a1ba2c29889680203d8ce2f0066f5335a8167f9.html



# 耐心配置Jenkins全局连接Gitee

https://blog.csdn.net/Xin_101/article/details/124628021



# docker+jenkins+docker-nginx镜像部署前端项目

https://zhuanlan.zhihu.com/p/493886079