# Docker 操作手册

[TOC]

## 一、简介

Docker 是一个开源的应用容器引擎，基于 [Go 语言](https://www.runoob.com/go/go-tutorial.html) 并遵从 Apache2.0 协议开源。

Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app）,更重要的是容器性能开销极低。

访问镜像库地址：https://hub.docker.com



## 二、Docker的应用场景

- Web 应用的自动化打包和发布。
- 自动化测试和持续集成、发布。
- 在服务型环境中部署和调整数据库或其他的后台应用。
- 从头编译或者扩展现有的 OpenShift 或 Cloud Foundry 平台来搭建自己的 PaaS 环境。



## 三、优点

1、快速，一致地交付您的应用程序

2、响应式部署和扩展

3、在同一硬件上运行更多工作负载



## 四、Docker 架构

基本概念

- **镜像（Image）**：Docker 镜像（Image），就相当于是一个 root 文件系统。比如官方镜像 ubuntu:16.04 就包含了完整的一套 Ubuntu16.04 最小系统的 root 文件系统。

- **容器（Container）**：镜像（Image）和容器（Container）的关系，就像是面向对象程序设计中的类和实例一样，镜像是静态的定义，容器是镜像运行时的实体。容器可以被创建、启动、停止、删除、暂停等。

- **仓库（Repository）**：仓库可看着一个代码控制中心，用来保存镜像。

  

  

  ![img](docker操作手册.assets/576507-docker1.png)

| 概念                   | 说明                                                         |
| :--------------------- | :----------------------------------------------------------- |
| Docker 镜像(Images)    | Docker 镜像是用于创建 Docker 容器的模板，比如 Ubuntu 系统。  |
| Docker 容器(Container) | 容器是独立运行的一个或一组应用，是镜像运行时的实体。         |
| Docker 客户端(Client)  | Docker 客户端通过命令行或者其他工具使用 Docker SDK (https://docs.docker.com/develop/sdk/) 与 Docker 的守护进程通信。 |
| Docker 主机(Host)      | 一个物理或者虚拟的机器用于执行 Docker 守护进程和容器。       |
| Docker 仓库(Registry)  | Docker 仓库用来保存镜像，可以理解为代码控制中的代码仓库。Docker Hub([https://hub.docker.com](https://hub.docker.com/)) 提供了庞大的镜像集合供使用。一个 Docker Registry 中可以包含多个仓库（Repository）；每个仓库可以包含多个标签（Tag）；每个标签对应一个镜像。通常，一个仓库会包含同一个软件不同版本的镜像，而标签就常用于对应该软件的各个版本。我们可以通过 **<仓库名>:<标签>**的格式来指定具体是这个软件哪个版本的镜像。如果不给出标签，将以 **latest** 作为默认标签。 |
| Docker Machine         | Docker Machine是一个简化Docker安装的命令行工具，通过一个简单的命令行即可在相应的平台上安装Docker，比如VirtualBox、 Digital Ocean、Microsoft Azure。 |



## 五、基本操作

### 1）centos 安装 Docker

> 一键安装安装命令如下：
>
> ```
> curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
> ```



#### A.删除Docker依赖包

如果需要卸载旧版本docker，则删除所有依赖包

```
$ sudo yum remove docker \
         docker-client \
         docker-client-latest \
         docker-common \
         docker-latest \
         docker-latest-logrotate \
         docker-logrotate \
         docker-engine
```



#### B.下载安装 Docker 相关工具

```
# yum-utils 提供了 **yum-config-manager** ，并且 device mapper 存储驱动程序需要 device-mapper-persistent-data 和 lvm2。
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
  
# 安装最新版本的 Docker Engine-Community 和 containerd。
sudo yum install docker-ce docker-ce-cli containerd.io
```



#### C.配置国内源

（直接执行安装docker命令会很慢。配置国内的源可以解决问题-阿里云的源。）

```
sudo yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```



#### D.启动 Docker

```
sudo systemctl start docker
```

通过运行 hello-world 映像来验证是否正确安装了 Docker Engine-Community 。

Docker 首先从本地主机上查找镜像是否存在，如果不存在，Docker 就会从镜像仓库 Docker Hub 下载公共镜像，并**运行**。

```
sudo docker run hello-world
```

docker 服务器开机自启动：

```
# systemctl enable 将服务设置为每次开机启动
# systemctl start 服务立即启动 下次不启动
# systemctl enable --now 立即启动且每次重启也启动

systemctl is-enabled docker.service #检查服务是否开机启动

systemctl enable docker.service --now #将服务配置成开机启动

systemctl start docker.service #启动服务
```



### 2）Docker 基本命令

基本上所有需要containID作为标识的，也可以通过name作为参数

![image-20191227113524342](docker操作手册.assets/image-20191227113524342.png)

#### A. 拉取镜像

如果我们本地没有 ubuntu 镜像，我们可以使用 docker pull 命令来载入 ubuntu 镜像

```
docker pull ubuntu
```



#### B. 新建一个后台运行的容器 

centos 镜像启动一个容器，参数为以命令行模式进入该容器。

参数说明

- **-i**: 交互式操作。
- **-t**: 终端。
- **-d:** 后台运行，去掉即直接进入容器内。
- **-P: **将容器内部使用的网络端口映射到我们使用的主机上。
- **-p:** 参数来设置不一样的端口=>  -p 5000:5000
- **--name:** 重命名
- **centos**: centos镜像。
- **/bin/bash**：放在镜像名后的是命令，这里我们希望有个交互式 Shell，因此用的是 /bin/bash。

```
docker run -itd --name web_project -p 8080:8080 centos /bin/bash
```



#### C. 退出容器

```
exit
```



#### D. 查看正在运行的容器

```
ps
```



#### E. 查看所有的容器

```
docker ps -a
```



#### F. 启动容器

```
docker start {containID} 
```

![image-20191227110906805](docker操作手册.assets/image-20191227110906805.png)



#### G. 停止容器

```
docker stop {containID}
```



#### H. 进入后台运行的程序

ps：exit退出后，程序仍后台运行。

```
docker exec -it {containID} /bin/bash
```



#### Y. 删除容器

```
docker rm -f {containID}
```



#### J. 导出容器生成镜像

```
docker export {containID} > ubuntu.tar
```



#### K. 导入镜像

ps: 也可走网络，具体百度。

```
cat docker/ubuntu.tar | docker import - test/ubuntu:v1
```



#### L. 查看本地镜像

```
docker images
```



#### M.进入容器

```
docker exec -it {containID} /bin/bash  
```



### 3）不常用指令

#### A. 查看容器网络端口映射

ps: 可通过docker ps 查看

```
docker port {containID}
```



#### B. 查看日志

- **-f:** 让 **docker logs** 像使用 **tail -f** 一样来输出容器内部的标准输出。

```
docker logs -f {containID}
```



#### C. 检查容器内部的进程

我们还可以使用 docker top 来查看容器**内部运行的进程**

```
docker top {containID}
```



#### D. Docker 的底层信息

```
docker inspect {containID}
```



#### E.容器开机启动

```
# 创建容器时候指定restart参数：
docker run    -it -p 6379:6379 --restart=always  --name redis -d redis

# 对已经创建的容器用docker update 更新:
docker update --restart=always  xxx
```

--restart具体参数值详细信息 :

1. no - 容器退出时，不重启容器
2. on-failure - 只有在非0状态退出时才从新启动容器
3. always - 无论退出状态是如何，都重启容器