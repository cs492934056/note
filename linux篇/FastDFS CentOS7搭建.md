# FastDFS CentOS搭建

## 按照步骤一步步往下执行指令即可



## 准备一个安装根目录 /usr/local/src/fastDFS

**注意！！！**	 后面很多地方都是以这个根目录为基础的，比如创建文件夹等。。

```
cd /usr/local/src
mkdir fastDFS
cd fastDFS
```









### 1.检查是否安装gcc

```yaml
whereis gcc
```

如果没安装，则安装 gcc , 安装了就跳过这个

```yaml
yum -y install gcc-c++
```



### 2.安装libevent

这是FastDFS依赖libevent库

```
yum -y install libevent
```



### 3.安装libfastcommon

```
wget -O libfastcommon-1.0.39.tar.gz
```

**或**

到 https://codeload.github.com/happyfish100/libfastcommon/tar.gz/V1.0.39 下载，使用xftp上传至安装根目录

```yaml
# 解压
tar xzvf libfastcommon-1.0.43.tar.gz

# 删除压缩包
rm -f libfastcommon-1.0.43.tar.gz

# 进入解压目录
cd libfastcommon-1.0.43

# 编译
./make.sh

# 安装
./make.sh  install
```



### 4.安装FastDFS

```
wget -O fastdfs-5.11.tar.gz https://codeload.github.com/happyfish100/fastdfs/tar.gz/V5.11
```

或

自行去 <https://github.com/happyfish100/fastdfs/releases> 下载，使用xftp上传至

```yaml
# 解压
tar xzvf fastdfs-5.11.tar.gz

# 删除
rm -f fastdfs-5.11.tar.gz

# 进入
cd fastdfs-5.11

# 编译
./make.sh

# 安装
./make.sh  install
```



默认安装方式，安装后的相应文件与目录位置,【仅作了解，没有要执行的指令】

> A、服务脚本：
>
> /etc/init.d/fdfs_storaged
> /etc/init.d/fdfs_trackerd
>
> B、配置文件（这三个是作者给的样例配置文件）
>
> /etc/fdfs/client.conf.sample
> /etc/fdfs/storage.conf.sample
> /etc/fdfs/tracker.conf.sample
>
> C、命令工具在 /usr/bin/ 目录下：
>
> fdfs_appender_test
> fdfs_appender_test1
> fdfs_append_file
> fdfs_crc32
> fdfs_delete_file
> fdfs_download_file
> fdfs_file_info
> fdfs_monitor
> fdfs_storaged
> fdfs_test
> fdfs_test1
> fdfs_trackerd
> fdfs_upload_appender
> fdfs_upload_file
> stop.sh
> restart.sh



### 5.配置FastDFS跟踪器(Tracker)

`1`创建文件夹

```
mkdir tracker
```



`2`进入 /etc/fdfs，复制 FastDFS 跟踪器样例配置文件 tracker.conf.sample，并重命名为 tracker.conf

```yaml
# 进入目录
cd /etc/fdfs/

# 复制
cp tracker.conf.sample tracker.conf

# 修改
vim tracker.conf
```



`3`配置修改以下内容，其他都可默认

```properties
# Tracker 数据和日志目录地址(根目录必须存在,子目录会自动创建)
base_path=/usr/local/src/fastDFS/tracker

# HTTP 服务端口 默认8080 ，[建议修改] 防止冲突
http.server_port=9080
```



`4`启动

```yaml
# CentOs推荐启动方式
systemctl start fdfs_trackerd
```

其他启动方式：

> /etc/init.d/fdfs_trackerd start
>
> service fdfs_trackerd start



`5`查看是否启动成功

```
systemctl status fdfs_trackerd
```



`6`设置开机启动

```
systemctl enable fdfs_trackerd.service
```

其他方式

> chkconfig fdfs_trackerd on



Tracker服务启动成功后，会在base_path下创建data、logs两个目录。目录结构如下：

> ${base_path}
> |__data
> | |__storage_groups.dat：存储分组信息
> | |__storage_servers.dat：存储服务器列表
> |__logs
> | |__trackerd.log： tracker server 日志文件



### 6.配置 FastDFS 存储 (Storage)

`1`创建文件夹

```
mkdir -p storage/base
```



`2`进入 /etc/fdfs，复制 FastDFS 跟踪器样例配置文件 tracker.conf.sample，并重命名为 tracker.conf

```yaml
# 进入目录
cd /etc/fdfs/

# 复制
cp storage.conf.sample storage.conf

# 修改
vim storage.conf
```



`3`配置内容如下：

```yaml
# Storage 数据和日志目录地址(根目录必须存在，子目录会自动生成)  (注 :这里不是上传的文件存放的地址,之前版本是的,在某个版本后更改了)
base_path=/usr/local/src/fastDFS/storage/base

# 逐一配置 store_path_count 个路径，索引号基于 0。
# 如果不配置 store_path0，那它就和 base_path 对应的路径一样。
store_path0=/usr/local/src/fastDFS/storage

# tracker_server 的列表 ，会主动连接 tracker_server
# 有多个 tracker server 时，每个 tracker server 写一行
# 这里只能配置外网IP , 否则【无法启动】
tracker_server=47.110.37.77:22122

# 访问端口 默认80  建议修改 防止冲突
http.server_port=9888
```



`4`启动(这个不行用那个，灵活把握)

```
# CentOs推荐启动方式
systemctl start fdfs_storaged
```

其他启动方式：

> /etc/init.d/fdfs_storaged start
>
> service fdfs_storaged start



`5`查看 Storage 是否成功启动，

```
netstat -unltp|grep fdfs
```



`6`设置开机启动

```
systemctl enable fdfs_storaged.service
```

其他方式

> chkconfig fdfs_storaged on





`7`查看Storage和Tracker是否在通信

```
/usr/bin/fdfs_monitor /etc/fdfs/storage.conf
```





Storage 目录

同 Tracker，Storage 启动成功后，在base_path 下创建了data、logs目录，记录着 Storage Server 的信息。
在 store_path0/data 目录下，创建了N*N个子目录：

```js
[root@localhost ~]# ls /fastdfs/storage/data/
00  05  0A  0F  14  19  1E  23  28  2D  32  37  3C  41  46  4B  50  55  5A  5F  64  69  6E  73  78  7D  82  87  8C  91  96  9B  A0  A5  AA  AF  B4  B9  BE  C3  C8  CD  D2  D7  DC  E1  E6  EB  F0  F5  FA  FF
01  06  0B  10  15  1A  1F  24  29  2E  33  38  3D  42  47  4C  51  56  5B  60  65  6A  6F  74  79  7E  83  88  8D  92  97  9C  A1  A6  AB  B0  B5  BA  BF  C4  C9  CE  D3  D8  DD  E2  E7  EC  F1  F6  FB
02  07  0C  11  16  1B  20  25  2A  2F  34  39  3E  43  48  4D  52  57  5C  61  66  6B  70  75  7A  7F  84  89  8E  93  98  9D  A2  A7  AC  B1  B6  BB  C0  C5  CA  CF  D4  D9  DE  E3  E8  ED  F2  F7  FC
03  08  0D  12  17  1C  21  26  2B  30  35  3A  3F  44  49  4E  53  58  5D  62  67  6C  71  76  7B  80  85  8A  8F  94  99  9E  A3  A8  AD  B2  B7  BC  C1  C6  CB  D0  D5  DA  DF  E4  E9  EE  F3  F8  FD
04  09  0E  13  18  1D  22  27  2C  31  36  3B  40  45  4A  4F  54  59  5E  63  68  6D  72  77  7C  81  86  8B  90  95  9A  9F  A4  A9  AE  B3  B8  BD  C2  C7  CC  D1  D6  DB  E0  E5  EA  EF  F4  F9  FE
```



`8`修改 Tracker 服务器中的客户端配置文件 

创建 client目录

```
cd /usr/local/src/fastDFS
mkdir client
```



```yaml
# 进入目录
cd /etc/fdfs

# 复制
cp client.conf.sample client.conf

# 打开编辑
vim client.conf
```



`9`配置内容如下：其他默认即可

```yaml
# Client 的数据和日志目录
base_path=/usr/local/src/fastDFS

# Tracker端口 ， 因为没有开放 22122 端口，这里只能配 127.0.0.1 !!! [坑不坑！]
tracker_server=127.0.0.1:22122
```





### 7.内部上传测试

只需要修改最后面的文件路径，文件确实存在才行

```
/usr/bin/fdfs_upload_file /etc/fdfs/client.conf /home/test.jpg
```



上传成功，返回文件ID号：group1/M00/00/00/wKgz6lnduTeAMdrcAAEoRmXZPp870.jpg

返回的文件ID由group、存储目录、两级子目录、fileid、文件后缀名（由客户端指定，主要用于区分文件类型）拼接而成。



例如：`group1/M00/00/00/wKgz6lnduTeAMdrcAAEoRmXZPp870.jpg`这样的路径，可以这样去找文件：=>

```
cd /usr/local/src/fastDFS/storage/data/00/00
ll
```



### 8.配置Nginx ，http访问文件

vim /usr/local/nginx/conf/nginx.conf

配置如下

```json

server {
        listen       8081;
        server_name  192.168.0.200;
 
        location /group1/M00{
        	alias /usr/local/fastDFS/storage/data/;
         	autoindex on;
       	}
 
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
        root   html;
       }
    }
```



**这个是重点**，其实就这一句

>location /group1/M00{
>​       	alias /usr/local/fastDFS/storage/data/;
>​        autoindex on;
>}