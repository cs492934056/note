### 一、nginx.conf主配置文件

- 配置基础nginx配置

- 每一个域名对应一个配置文件，实现模块化处理，其中，根域名配置在nginx.conf，其他子域名则配置 **./vhost **文件夹中，引入代码在如下配置文件中  include /usr/local/nginx/conf/vhost/*.conf;

- 由于采用了https 服务需要配置SSL证书，已将阿里云生成的免费证书放在 **./cert** 文件夹中![image-20201010232020211](nginx配置.assets/image-20201010232020211.png)

  

```javascript
#user  nobody;
worker_processes  2;

error_log  logs/error.log error;
pid        logs/nginx.pid;

events {
    accept_mutex on;   #设置网路连接序列化，防止惊群现象发生，默认为on
    multi_accept on;  #设置一个进程是否同时接受多个网络连接，默认为off
    #use epoll;      #事件驱动模型，select|poll|kqueue|epoll|resig|/dev/poll|eventport
    worker_connections  1024;    #最大连接数，默认为512
}

http {
    include       mime.types;
    default_type  application/octet-stream;

    #access_log off; #取消服务日志    
    log_format myFormat '$remote_addr–$remote_user [$time_local] $request $status $body_bytes_sent $http_referer $http_user_agent $http_x_forwarded_for'; #自定义格式
    access_log logs/access.log myFormat;  #combined为日志格式的默认值

    sendfile off;   #允许sendfile方式传输文件，默认为off，可以在http块，server块，location块。
    sendfile_max_chunk 100k;  #每个进程每次调用传输数量不能大于设定的值，默认为0，即不设上限。
    keepalive_timeout 65;  #连接超时时间，默认为75s，可以在http，server，location块。

    ##设定负载均衡的服务器列表
    # upstream mysvr {
    ##weigth参数表示权值，权值越高被分配到的几率越大
    ##本机上的Squid开启3128端口
    #server 192.168.8.1:3128 weight=5;
    #server 192.168.8.2:80  weight=1;
    #server 192.168.8.3:80  weight=6;
    #}

    # 开启gzip功能
    gzip on;
    # 配置nginx使用缓存空间的大小：16指的是缓存空间的个数，8K为单个缓存空间的大小
    # 从nginx0.7.28后，默认缓存空间大小为128k：这里是16*8K=128K
    gzip_buffers 16 8K;
    # 指定压缩级别：1-9；1压缩程度最低，效率最高，9压缩程度最高，效率最低
    gzip_comp_level 3;
    # 针对某些User_agent关闭gzip功能，后接的是正则表达式
    # 这里以MSIE 4/5/6的浏览器为例开启gzip功能
    gzip_disable MISE [4-6]\.;
    # 指定特定http协议版本(1.0或1.1)，选择性开启gzip功能
    gzip_http_version 1.1;
    # 根据响应页面的大小选择性关闭gzip(比较重要)，大于才开启
    # 防止出现压缩很小的数据出现压缩后数据量变大的情况,建议设置为1K或以上
    gzip_min_length 1024;
    # 根据网页的MIME类型选择性改期gzip
    gzip_types text/plain application/x-javascript text/css application/xml image/jpeg image/gif image/png;
    # 使用gzip时是否发送带有“Vary:Accept-Encoding”头域的响应头部
    gzip_vary on;
    # 在客户端不支持Gzip压缩时，服务器将返回解压后的数据；客户端支持gzip，该参数被忽略，返回的是压缩后的数据
    # gunzip_static on;

	######################################################################
    # yueyueniao.cn
	server {
		listen 80;
		server_name yueyueniao.cn;   #将localhost修改为您证书绑定的域名，例如：www.example.com。
		rewrite ^(.*)$ https://$host$1 permanent;   #将所有http请求通过rewrite重定向到https。
		location / {
			index index.html index.htm;
		}
	}
    server {
        add_header Access-Control-Allow-Origin "*";
        server_name yueyueniao.cn;
        
        # https ssl配置
        listen 443 ssl;
        listen [::]:443 ssl;
        ssl_certificate   cert/4410298_yueyueniao.cn.pem;
        ssl_certificate_key  cert/4410298_yueyueniao.cn.key;
        ssl_session_timeout 5m;
        ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;

        # 根目录
        location / {
            index index.html index.htm index.php;
            root /yyn/pc/yyn_offical;
        }

        # 图片，默认放在/yyn/pic
        location /imgs {
            index index.html index.htm index.php;
            alias /yyn/imgs/;
        }

        location /penk/ {
            rewrite ^/penk/(.*)$ /$1 break;
            proxy_pass http://localhost:8078;
            proxy_http_version 1.1;
            proxy_set_header Connection "";
            proxy_set_header  Host $host;
            proxy_set_header   X-Real-IP        $remote_addr;
            proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
            proxy_next_upstream off;
        }
    }

    # 导入所有的conf
    include /usr/local/nginx/conf/vhost/*.conf;
}

```



### 二、扩展配置子模块

- 子模块情况

![image-20201010232203571](nginx配置.assets/image-20201010232203571.png)

- 子模块配置

```javascript
# back.yueyueniao.cn
server {
	listen 80;
	server_name back.yueyueniao.cn;   #将localhost修改为您证书绑定的域名，例如：www.example.com。
	rewrite ^(.*)$ https://$host$1 permanent;   #将所有http请求通过rewrite重定向到https。
	location / {
		index index.html index.htm;
	}
}

server {
	add_header Access-Control-Allow-Origin "*";
	server_name back.yueyueniao.cn;
        
	# https ssl配置
	listen 443 ssl;
	listen [::]:443 ssl;
                ssl_certificate  /path.pem;      # pem的位置
                ssl_certificate_key  /path.key;  # key的位置
	ssl_session_timeout 5m;
	ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_prefer_server_ciphers on;

	# 根目录
	location / {
		index index.html index.htm index.php;
		root /penk/penk-web-master;
	}

	location /penk/ {
		rewrite ^/penk/(.*)$ /$1 break;
		proxy_pass http://localhost:8888;
		proxy_http_version 1.1;
		proxy_set_header Connection "";
		proxy_set_header  Host $host;
		proxy_set_header   X-Real-IP        $remote_addr;
		proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
		proxy_next_upstream off;
	}
}

```



### 三、证书

证书存放路径如下，只是为了方便管理~

![image-20201010232648031](nginx配置.assets/image-20201010232648031.png)



### 四、重启生效

```javascript
# 找到nginx
运行文件目录/nginx -reload -s; 
```

