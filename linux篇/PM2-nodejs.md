# PM2 nodejs应用进程管理器



## [欢迎！](https://pm2.keymetrics.io/docs/usage/quick-start/#welcome)

欢迎使用PM2快速入门！

PM2是守护程序进程管理器，它将帮助您管理和保持应用程序在线。PM2入门非常简单，它是一个简单直观的CLI，可以通过NPM安装。

## [安装](https://pm2.keymetrics.io/docs/usage/quick-start/#installation)

最新的PM2版本可通过NPM或Yarn安装：

```
$ npm install pm2@latest -g
# or
$ yarn global add pm2
```

要安装Node.js和NPM，可以使用[NVM](https://yoember.com/nodejs/the-best-way-to-install-node-js/)

## [启动一个应用](https://pm2.keymetrics.io/docs/usage/quick-start/#start-an-app)

启动，守护和监视应用程序的最简单方法是使用以下命令行：

```
$ pm2 start app.js
```

或轻松启动任何其他应用程序：

```
$ pm2 start bashscript.sh
$ pm2 start python-app.py --watch
$ pm2 start binary-file -- --port 1520
```

可以传递给CLI的一些选项：

```
# Specify an app name
--name <app_name>

# Watch and Restart app when files change
--watch

# Set memory threshold for app reload
--max-memory-restart <200MB>

# Specify log file
--log <log_path>

# Pass extra arguments to the script
-- arg1 arg2 arg3

# Delay between automatic restarts
--restart-delay <delay in ms>

# Prefix logs with time
--time

# Do not auto restart app
--no-autorestart

# Specify cron for forced restart
--cron <cron_pattern>

# Attach to application log
--no-daemon
```

如您所见，许多选项可用于通过PM2管理您的应用程序。您将根据您的用例发现它们。

## [管理流程](https://pm2.keymetrics.io/docs/usage/quick-start/#managing-processes)

管理应用程序状态很简单，以下是命令：

```
$ pm2 restart app_name
$ pm2 reload app_name
$ pm2 stop app_name
$ pm2 delete app_name
```

而不是`app_name`您可以通过：

- `all` 在所有过程中采取行动
- `id` 对特定的进程ID采取行动

## [检查状态，日志，指标](https://pm2.keymetrics.io/docs/usage/quick-start/#check-status-logs-metrics)

现在，您已经启动了该应用程序，可以检查其状态，日志，指标，甚至可以通过[pm2.io](https://pm2.io/)获取在线仪表板。

### [列出托管的应用程序](https://pm2.keymetrics.io/docs/usage/quick-start/#list-managed-applications)

列出由PM2管理的所有应用程序的状态：

```
$ pm2 [list|ls|status]
```

![https://i.imgur.com/LmRD3FN.png](https://i.imgur.com/LmRD3FN.png)

### [显示日志](https://pm2.keymetrics.io/docs/usage/quick-start/#display-logs)

要实时显示日志：

```
$ pm2 logs
```

要挖掘较旧的日志，请执行以下操作：

```
$ pm2 logs --lines 200
```

### [基于终端的仪表板](https://pm2.keymetrics.io/docs/usage/quick-start/#terminal-based-dashboard)

这是一个直接适合您终端的实时仪表板：

```
$ pm2 monit
```

![https://i.imgur.com/xo0LDb7.png](https://i.imgur.com/xo0LDb7.png)

### [pm2.io：监视和诊断Web界面](https://pm2.keymetrics.io/docs/usage/quick-start/#pm2io-monitoring--diagnostic-web-interface)

基于Web的仪表板，带有诊断系统的跨服务器：

```
$ pm2 plus
```

![https://i.imgur.com/sigMHli.png](https://i.imgur.com/sigMHli.png)

## [集群模式](https://pm2.keymetrics.io/docs/usage/quick-start/#cluster-mode)

对于Node.js应用程序，PM2包含一个自动负载平衡器，它将在每个衍生进程之间共享所有HTTP [s] / Websocket / TCP / UDP连接。

要以“群集”模式启动应用程序，请执行以下操作：

```
$ pm2 start app.js -i max
```

[在此处](https://pm2.keymetrics.io/docs/usage/cluster-mode/)阅读有关集群模式的更多信息。

## [生态系统文件](https://pm2.keymetrics.io/docs/usage/quick-start/#ecosystem-file)

您还可以创建一个配置文件，称为生态系统文件，以管理多个应用程序。生成生态系统文件：

```
$ pm2 ecosystem
```

这将生成和生态系统.config.js文件：

```
module.exports = {
  apps : [{
    name: "app",
    script: "./app.js",
    env: {
      NODE_ENV: "development",
    },
    env_production: {
      NODE_ENV: "production",
    }
  }, {
     name: 'worker',
     script: 'worker.js'
  }]
}
```

并轻松启动：

```
$ pm2 start process.yml
```

[在此处](https://pm2.keymetrics.io/docs/usage/application-declaration/)阅读有关应用程序声明的更多信息。

## [设置启动脚本](https://pm2.keymetrics.io/docs/usage/quick-start/#setup-startup-script)

使用服务器引导/重新引导中管理的进程重新启动PM2至关重要。要解决此问题，只需运行以下命令即可生成活动的启动脚本：

```
$ pm2 startup
```

并冻结自动重生的进程列表：

```
$ pm2 save
```

[在此处](https://pm2.keymetrics.io/docs/usage/startup/)阅读有关启动脚本生成器的更多信息。

## [更改后重新启动应用程序](https://pm2.keymetrics.io/docs/usage/quick-start/#restart-application-on-changes)

使用该`--watch`选项非常简单：

```
$ cd /path/to/my/app
$ pm2 start env.js --watch --ignore-watch="node_modules"
```

这将监视并重新启动应用程序，以解决当前目录+所有子文件夹中的任何文件更改，并且将忽略node_modules文件夹中的任何更改`--ignore-watch="node_modules"`。

然后，您可以`pm2 logs`用来检查重新启动的应用程序日志。

## [更新PM2](https://pm2.keymetrics.io/docs/usage/quick-start/#updating-pm2)

我们很简单，版本之间没有重大变化，过程很简单：

```
npm install pm2@latest -g
```

然后更新内存中的PM2：

```
pm2 update
```

## [备忘单](https://pm2.keymetrics.io/docs/usage/quick-start/#cheatsheet)

以下是一些值得了解的命令。只需使用示例应用程序或开发计算机上的当前Web应用程序来尝试使用它们：

```
# Fork mode
pm2 start app.js --name my-api # Name process

# Cluster mode
pm2 start app.js -i 0        # Will start maximum processes with LB depending on available CPUs
pm2 start app.js -i max      # Same as above, but deprecated.
pm2 scale app +3             # Scales `app` up by 3 workers
pm2 scale app 2              # Scales `app` up or down to 2 workers total

# Listing

pm2 list               # Display all processes status
pm2 jlist              # Print process list in raw JSON
pm2 prettylist         # Print process list in beautified JSON

pm2 describe 0         # Display all informations about a specific process

pm2 monit              # Monitor all processes

# Logs

pm2 logs [--raw]       # Display all processes logs in streaming
pm2 flush              # Empty all log files
pm2 reloadLogs         # Reload all logs

# Actions

pm2 stop all           # Stop all processes
pm2 restart all        # Restart all processes

pm2 reload all         # Will 0s downtime reload (for NETWORKED apps)

pm2 stop 0             # Stop specific process id
pm2 restart 0          # Restart specific process id

pm2 delete 0           # Will remove process from pm2 list
pm2 delete all         # Will remove all processes from pm2 list

# Misc

pm2 reset <process>    # Reset meta data (restarted time...)
pm2 updatePM2          # Update in memory pm2
pm2 ping               # Ensure pm2 daemon has been launched
pm2 sendSignal SIGUSR2 my-app # Send system signal to script
pm2 start app.js --no-daemon
pm2 start app.js --no-vizion
pm2 start app.js --no-autorestart
```

## [下一步是什么？](https://pm2.keymetrics.io/docs/usage/quick-start/#whats-next)

了解如何将应用程序的所有行为选项声明为[JSON配置文件](http://pm2.keymetrics.io/docs/usage/application-declaration/)。

了解如何进行[清洁停止并重新启动](http://pm2.keymetrics.io/docs/usage/signals-clean-restart/)以提高可靠性。

了解如何[轻松部署和更新生产应用程序](http://pm2.keymetrics.io/docs/usage/deployment/)。

使用[Keymetrics](https://keymetrics.io/)监视您的生产应用程序。

## [如何更新PM2](https://pm2.keymetrics.io/docs/usage/quick-start/#how-to-update-pm2)

安装最新的pm2版本：

```
npm install pm2@latest -g
```

然后更新内存中的PM2：

```
pm2 update
```