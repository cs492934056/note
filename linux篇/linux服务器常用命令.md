```javascript
# 查看相关的进程
# ps -aux | grep {进程名}
ps -aux | grep java
```



```javascript
# 查看端口号占用
# lsof -i:{端口号}
lsof -i:3306

# netstat  -anp | grep {端口号}
netstat  -anp | grep 3306
```



```javascript
# 开机自启动
# centos系统 /etc/rc.d/rc.local 追加需要的脚本即可
```



```javascript
# windows 测试服务器端口
# telnet {ip} {port}
telnet 120.77.204.132 22
```



```javascript
# 统计大小
# du -sh {path}
du -sh /root
```

