### 1、安装docker

### 2、[安装nextcloud](https://hub.docker.com/_/nextcloud)



> ###### [up主视频一条龙](https://www.bilibili.com/video/BV1Bt4y197Ay)
> 配置了docker-compose.yaml，自动下载mysql以及nextcloud。
> 测试环境配置好nginx即可~




## 遇到的难点

1、生产环境需要配置SSL，可以不用改动docker中的配置
2、确定docker容器启动没问题，配置nginx

3、可以进入创建管理员页面，数据库主机名以及端口，可以填写**docker-compose.yaml**中的数据库名,即db