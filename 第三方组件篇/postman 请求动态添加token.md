# postman 请求动态添加token

### 1、添加一个Collection



![](postman 请求动态添加token.assets\1608703258179.png)

![1608703241851](postman 请求动态添加token.assets\1608703241851.png)



### 2、添加文件夹，每个文件夹对应一个模块

![](postman 请求动态添加token.assets\1608703339878.png)

添加后如下

![1608703392400](postman 请求动态添加token.assets\1608703392400.png)



### 3、添加环境变量

![1608703454142](postman 请求动态添加token.assets\1608703454142.png)

![1608703467691](postman 请求动态添加token.assets\1608703467691.png)

其中，base_url为url请求前缀； authorization 为请求所需的token。



### 4、添加请求

登录请求接口，{{base_url}}代表环境变量，POST请求，body中带账户密码参数。

![1608703730238](postman 请求动态添加token.assets\1608703730238.png)



**重点：tests**

把请求返回的token，赋值到环境变量中。

![1608703922803](postman 请求动态添加token.assets\1608703922803.png)



```javascript
var data = JSON.parse(responseBody);
if (data.data.token) {
  tests['Zyp ' +data.data.token] = true;
   postman.setEnvironmentVariable("authorization", 'Zyp ' +data.data.token);
}
else {
  tests["Body has token"] = false;
}
```



### 5、修改请求头

编辑整个项目

![1608704047250](postman 请求动态添加token.assets\1608704047250.png)



添加头部认证

![1608704070460](postman 请求动态添加token.assets\1608704070460.png)



### 测试

先登录，获取token

![1608704402935](postman 请求动态添加token.assets\1608704402935.png)

发送其他需要token的请求

![1608704478721](postman 请求动态添加token.assets\1608704478721.png)