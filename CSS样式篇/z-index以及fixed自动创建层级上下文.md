## z-indx以及fixed自动创建层级上下文

### 一、简介

z-index用来改变元素的层级关系。使用z-index首先需要设置position属性，可以是relative、absolute、fixed，只有当元素发生重叠是z-index的效果才会显现出来；但是，仅仅设置了position、z-index及其位置属性并不一定会达到想要的效果。

文章是模拟vue框架中使用bootstrap弹框遮罩以及vue-fullscreen全屏效果出来的问题，进行总结分析。



bootstrap官网注释：

![image-20200917085249390](z-index.assets/image-20200917085249390.png)

![image-20200917085340546](z-index.assets/image-20200917085340546.png)



### 二、场景

#### 场景一

- 场景分析：

在box框中，有一个dialog弹框，当弹框出来时候，会有modal 遮罩出现。如上图与代码，符合要求。

- 代码：

```
<!--
 * @Author: Penk
 * @LastEditors: Penk
 * @LastEditTime: 2020-09-16 17:06:36
 * @FilePath: \zhnyWeb_trunkc:\Users\admin\Desktop\1.html
 -->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>

  <style>
    * {
      margin: 0px;
      padding: 0px;
    }

    /* 居中样式 */
    .pk-display-flex-center {
      display: flex;
      flex-wrap: wrap;
      align-items: center;
      justify-content: center;
    }

    .box1 {
      position: relative;
      top: 0px;
      left: 0px;
      width: 300px;
      height: 300px;
      background-color: hotpink;
    }

    .box2 {
      position: fixed;
      top: 0px;
      left: 0px;
      bottom: 0px;
      right: 0px;
      background-color: hotpink;
    }
    
    /* html,
    body {
      height: 100%;
      position: relative;
    } */

    .box3 {
      position: absolute;
      top: 0px;
      left: 0px;
      bottom: 0px;
      right: 0px;
      background-color: hotpink;
    }

    .box4 {
      position: relative;
      height: 100%;
      width: 100%;
      background-color: hotpink;
    }

    .dialog {
      position: fixed;
      z-index: 1080;
      width: 100px;
      height: 100px;
      background-color: greenyellow;
    }

    .modal {
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      z-index: 1040;
      background-color: #000;
      opacity: .5;
    }
  </style>
</head>

<body>
  <div class="box box1 pk-display-flex-center">
    <div class="dialog"></div>
  </div>
  <div class="modal">

  </div>

  <div style="float:right;">
    <p>粉红色是主页</p>
    <p>灰色是遮罩</p>
    <p>绿色是弹框</p>
    <p>理想状态下，弹框在最顶层</p>
  </div>
</body>

</html>
```

- 展示：

![image-20200916165702628](z-index.assets/image-20200916165702628.png)





#### 场景二

- 场景解析：

甲方提需求，需要一个按钮或者事件触发，让box框（不包括其他）这个DIV全屏切换，我们可以通过切换box框的样式达到目的，即我们的.box2

- 部分代码（同上）：

```
.box2 {
      position: fixed;
      top: 0px;
      left: 0px;
      bottom: 0px;
      right: 0px;
      background-color: hotpink;
    }
    
// 其中html部分代码  
<div class="box box2 pk-display-flex-center">
    <div class="dialog"></div>
</div>
<div class="modal">
```

- 展示：

![image-20200916171207231](z-index.assets/image-20200916171207231.png)



### 三、问题

设置成了box2后，modal遮罩把dialog弹框遮挡住了，此时，z-index情况如下

```
box 无设置
dialog：1080
modal：1040
```

一般情况下，dialog在modal上方。



查阅资料，fixed会自动为div创建层级上下文，即

```
box 无设置（fixed,自动创建层级上下文）
dialog：1080（因为父级有层级，导致其失效）
modal：1040
```



### 四、解决以及思路

1. 从box样式入手

```
    .box3 {
      position: absolute;
      top: 0px;
      left: 0px;
      bottom: 0px;
      right: 0px;
      background-color: hotpink;
    }

    .box4 {
      position: relative;
      height: 100%;
      width: 100%;
      background-color: hotpink;
    }
    
    // 但是这些，需要其父级占据100%高宽
    html,
    body {
      height: 100%;
      position: relative;
    }
```

如果其结构复杂，可以考虑用js将其移到顶层，再进行操作。



2、 从modal遮罩入手

将modal移到box内部，与dialog同级即可

```
<div class="box box2 pk-display-flex-center">
    <div class="dialog"></div>
    <div class="modal"></div>
</div>
```



注：bootstrap 暂未找到可以追加到父元素上，element-ui可以！！！

1）element-ui dialog解决方案

![image-20200916174109580](z-index.assets/image-20200916174109580.png)

2）boostrap modal解决方案

```
// 定义全局函数，将遮罩.modal-backdrop 追加到指定的dom后边，方法参数可自行优化

function $modalDispose() {
  this.$nextTick(() => {
    setTimeout(() => {
      if ($(".modal-backdrop")[0]) {
        $(".modalPage")[0].append($(".modal-backdrop")[0]);
      }
    }, 500);
  })
}
```

