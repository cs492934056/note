## JS 与 JQuery 获取高度的方法以及的区别

> Js获取高度不能直接
>
> document.getElementById(XXX).height;



height：指元素内容的高度 ，[jQuery](http://lib.csdn.net/base/22)中的height()方法返回的就是这个高度。

clientHeight：内容高度+padding高度 ，jQuery中的innerHeight()方法返回的就是这个高度。

offsetHeight：内容高度+padding高度+边框宽度 ，jQuery中的outerHeight()方法返回的就是这个高度。



```html
<!--
 * @Author: Penk
 * @LastEditors: Penk
 * @LastEditTime: 2021-02-01 10:26:38
 * @FilePath: \penk-web-masterc:\Users\49293\Desktop\1 (2).html
-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.bootcss.com/jquery/3.5.1/jquery.min.js"></script>
  <title>Document</title>
  <style>
    body {
      margin-top: 50px;
      position: relative;
    }

    .title {
      width: 100%;
      font-size: 28px;
      font-weight: 600;
      line-height: 80px;
    }

    .paddingBox {
      width: 300px;
      height: 200px;
      padding: 20px;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
    }

    .paddingBox1 {
      position: absolute;
      top: 100px;
      left: 100px;
      background-color: yellowgreen;
    }

    .paddingBox2 {
      position: absolute;
      top: 500px;
      left: 100px;
      background-color: aqua;
    }
  </style>
</head>

<body>
  <div class="paddingBox paddingBox1" id="paddingBox1">
    <div class="title">document</div>
    <div class="height"></div>
    <div class="styleHeight"></div>
    <div class="getComputedStyleHeight"></div>
    <div class="clientHeight1"></div>
    <div class="offsetHeight1"></div>
  </div>
  <div class="paddingBox paddingBox2">
    <div class="title">jquery</div>
    <div class="height2"></div>
    <div class="clientHeight2"></div>
    <div class="offsetHeight2"></div>
  </div>
</body>

<script>
  window.onload = function () {
    let paddingBox1 = document.getElementsByClassName("paddingBox1")[0];


    // undefine 
    let height = paddingBox1.height;
    // 不是内联样式为空
    let styleHeight = paddingBox1.style.height;
    // 只能通过计算属性获取
    let getComputedStyleHeight = parseInt(window.getComputedStyle(paddingBox1).getPropertyValue('height'));
    let clientHeight = paddingBox1.clientHeight;
    let offsetHeight = paddingBox1.offsetHeight;


    document.getElementsByClassName("height")[0].innerHTML = "height：" + height;
    document.getElementsByClassName("styleHeight")[0].innerHTML = "styleHeight：" + styleHeight;
    document.getElementsByClassName("getComputedStyleHeight")[0].innerHTML = "getComputedStyleHeight：" + getComputedStyleHeight;
    document.getElementsByClassName("clientHeight1")[0].innerHTML = "clientHeight：" + clientHeight;
    document.getElementsByClassName("offsetHeight1")[0].innerHTML = "offsetHeight：" + offsetHeight;

    // jquery
    let jq_height = $('.paddingBox2').height();
    let jq_innerHeight = $('.paddingBox2').innerHeight();
    let jq_outerHeight = $('.paddingBox2').outerHeight();

    $('.height2').html("jq_height：" + jq_height);
    $('.clientHeight2').html("jq_innerHeight：" + jq_innerHeight);
    $('.offsetHeight2').html("jq_outerHeight：" + jq_outerHeight);
  }
</script>

</html>
```

