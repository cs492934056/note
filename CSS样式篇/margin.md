# CSS

## margin

> In this specification, the expression *collapsing margins* means that adjoining margins (no non-empty content, padding or border areas or clearance separate them) of two or more boxes (which may be next to one another or nested) combine to form a single margin. 所有毗邻的两个或更多盒元素的margin将会合并为一个margin共享之。毗邻的定义为：同级或者嵌套的盒元素，并且它们之间没有非空内容、Padding或Border分隔。



```
<div class="outter">
  <div class="inner"></div>
</div>   
.outter{
margin:20px;
}
.inner{
margin:50px;
}
```

嵌套的盒模型，会融合外边框，谁长就以谁为标准

解决方法：

1. outter添加padding
2. outter添加border
