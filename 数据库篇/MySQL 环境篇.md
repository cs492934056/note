# mysql 操作手册

[TOC]

## 一、环境简介

1. 查看内核/操作系统/CPU信息 

   ```
   uname -a
   ```

   ![image-20191224150759930](MySQL 环境篇.assets/image-20191224150759930.png) 

2. 查看CPU信息 

   ```
   cat /proc/cpuinfo
   ```

    ![image-20191224151312963](MySQL 环境篇.assets/image-20191224151312963.png)

3. 查看计算机内存

   ```
free
   ```
   
   ![image-20191224151446584](MySQL 环境篇.assets/image-20191224151446584.png) 

4. 查看端口号

   ```
   netstat -anp | grep 3306
   ```

   

## 二、mysql 服务脚本

1. mysql服务操作

   ```mysql
   # 查看mysql状态
   systemctl status mysql.service
   
   # 开启mysql
   systemctl start mysql.service
   
   # 关闭mysql
   systemctl stop mysql.service
   
   # 重启mysql
   systemctl restart mysql.service
   ```

2. 关闭防火墙

   ```mysql
   #（--permanent永久生效，没有此参数重启后失效）
   firewall-cmd --zone=public --add-port=3306/tcp --permanent 
   
   #重新载入
   firewall-cmd --reload
   
   #查看
   firewall-cmd --zone=public --query-port=3306/tcp
   
   vi /etc/sysconfig/iptables
   #添加
   -A RH-Firewall-1-INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT
   ```

6. 创建可远程连接用户

   ```mysql
   # CREATE USER '用户名'@'主机' IDENTIFIED BY '密码';
   CREATE USER 'penk'@'%' IDENTIFIED BY '123456';
   
   # mysql8.0默认的加密方式是“caching_sha2_password”，而navicat只支持以前的"mysql_native_password",所以接下来修改密码加密方式
   ALTER USER 'penk'@'%' IDENTIFIED WITH mysql_native_password BY '123456';
   
   # 设置该账户可以远程登陆
   GRANT ALL PRIVILEGES ON *.* TO 'penk'@'%';
   
   # 刷新权限
   flush privileges;
   ```

   

## 三、连接mysql方式

Mysql有两种连接方式：MySQL客户可以两种不同的方式连接mysqld服务器：Unix套接字，它通过在文件系统中的一个文件(缺省“/tmp/mysqld.sock”)进行连接；或TCP/IP，它通过一个端口号连接。

1. TCP/IP
2. socket 作用： 发起本地连接

mysql.sock是随每一次 mysql server启动生成的，因此mysql服务连接不上的根本原因还是配置文件设置出问题了。
MySQL下mysql.sock丢失丢失的原因一般是因为配置文件不一致的原因，mysqld 错误启动，mysqld_safe 会清除一次mysql.sock 。

## 四、mysql Bug

#### 	1) 登陆失败

win10环境使用Navicat 远程登陆不了阿里云服务器，便先用xsheel登陆服务器，输入指令后如下：![image-20191224151957855](MySQL 环境篇.assets/image-20191224151957855.png)

```
mysql -u root -p
```

> ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/var/lib/mysql/mysql.sock' (2)

查询mysql.sock是否存在

![image-20191224153234959](MySQL 环境篇.assets/image-20191224153234959.png)

```
find / -name mysql.sock
```

<u>**无数据!!!文件不存在!!!**</u>

由上可知是数据库配置问题，导致mysql.sock被删除，将socket注释，让其重新启动重新生成即可，具体如下：

![image-20191224163726626](MySQL 环境篇.assets/image-20191224163726626.png)

**<u>mysql 为客户端 mysqld 为服务端，后来发现是服务器上只有mysql 客户端，之前跑的docker 安装的mysql镜像，重启docker 即可，具体参考docker 篇。。。</u>**



