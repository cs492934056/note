# 1、全局级别

>  [] 表示可选可不选 {A|B} 表示选择A或者选择B

### 1.1  显示数据库服务器的状态信息

```mysql
SHOW STATUS；
```

### 1.2  显示授权用户的安全权限

```mysql
SHOW GRANTS；
```

### 1.3  修改加密规则

```mysql
ALTER USER 'root'@'localhost' IDENTIFIED BY 'password' PASSWORD EXPIRE NEVER;     
#修改加密规则
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '{NewPassword}'; 
#更新密码（mysql_native_password模式）
FLUSH PRIVILEGES;   
#刷新权限 
```

### 1.4  添加远程账户

```mysql
# 查看用户信息
select host,user,plugin,authentication_string from mysql.user;

# CREATE USER '用户名'@'主机' IDENTIFIED BY '密码';
CREATE USER 'penk'@'%' IDENTIFIED BY '123456';

# mysql8.0默认的加密方式是“caching_sha2_password”，而navicat只支持以前的"mysql_native_password",所以接下来修改密码加密方式
ALTER USER 'penk'@'%' IDENTIFIED WITH mysql_native_password BY '123456';

# 设置该账户可以远程登陆
GRANT ALL PRIVILEGES ON *.* TO 'penk'@'%';

# 刷新权限
flush privileges;

# 设置查看数据的编码格式
SET NAMES UTF8;
```



# 2、数据库级别

### 2.1  数据库操作

```mysql
# 显示所有数据库
SHOW DATABASES；

# 创建数据库
CREATE DATABASE db1 [IF NOT EXISTS db1] [CHARACTER SET GBK];

# 选择数据库
USE db1;

# 删除数据库
# DROP DATABASE db1；

# 修改数据库编码方式
ALTER DATABASE db1 CHARACTER SET utf8;

# 获取当前所选的数据库中所有可用的表，可先参考下方=>"数据表操作"
SHOW TABLES;

# 查看数据库的生成具体信息
SHOW CREATE tb1;
```

### 2.2  数据表操作

##### 2.2.1  单独一个表

> - 允许NULL值，则说明在插入行数据时允许不给出该列的值，而NOT NULL则表示在插入或者更新该列数据，必须明确给出该列的值；
> - DEFAULT表示该列的默认值，在插入行数据时，若没有给出该列的值就会使用其指定的默认值；
> - PRIMARY KEY用于指定主键，主键可以指定一列数据，而可以由多列数据组合构成，如PRIMARY KEY(cust_id,cust_name)；
> - ENGINE用于指定引擎类型。常见的引擎类型有这些：（1）InnoDB是一个支持可靠的事务处理的引擎，但是不支持全文本搜索；（2）MyISAM是一个性能极高的引擎，它支持全文本搜索，但是不支持事务处理；（3）MEMORY在功能上等同于MyISAM，但由于数据存储在内存中，速度很快（特别适合于临时表）；
>
> - 在创建表的时候可以使用FOREIGN KEY来创建外键，即一个表中的FOREIGN KEY指向另一个表中PRIMARY KEY。外键FOREIGN KEY用于约束破坏表的联结动作，保证两个表的数据完整性。同时也能防止非法数据插入外键列，因为该列值必须指向另一个表的主键。

```mysql
# 创建表可以使用CREATE TABLE语句
CREATE TABLE customers(
cust_id INT NOT NULL AUTO_INCREMENT,
cust_name CHAR(50) NOT NULL,
cust_age INT NULL DEFAULT 18,
PRIMARY KEY(cust_id)
)ENGINE=INNODB;

CREATE TABLE Orders(
Id_O int NOT NULL,
OrderNo int NOT NULL,
Id_P int,
PRIMARY KEY (Id_O),
FOREIGN KEY (Id_P) REFERENCES Persons(Id_P)
)

# 创建数据表，为下文增删改查数据准备
CREATE TABLE tb1 IF NOT EXISTS tb1(
	username VARCHAR(20),
	age TINYINT UNSIGNED,
	salary FLOAT(8,2) UNSIGNED
);

# 修改表名
# RENAME TABLE db1 TO db2;

### 添加列操作
### ALTER TABLE table_name ADD [COLUMN] col_name col_definition [FIRST | AFTER col_name]

# 添加列
ALTER TABLE tb1 ADD phone char(11) AFTER age;
ALTER TABLE tb1 ADD truename VARCHAR(10) FIRST;
# 修改列定义
ALTER TABLE tb1 MODIFY COLUMN phone char(12);
# 修改列名称+定义
ALTER TABLE tb1 CHANGE COLUMN phone my_phone char(12);
# 删除列
ALTER TABLE tb1 DROP my_phone;

# 添加一个自增字段，自增必须为主主键，主键会自动设置NOT NULL
ALTER TABLE tb1 ADD tb1_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY;
# 修改列为唯一键
ALTER TABLE tb1 MODIFY COLUMN username VARCHAR(20) UNIQUE KEY;
# 添加一个枚举类型，默认为3
ALTER TABLE tb1 ADD sex ENUM('1','2','3') DEFAULT '3';

# 显示表所有字段
SHOW COLUMNS FROM tb1；
DESC tb1;

# 删除表
DROP TABLE tb1;
```

##### 2.2.2  外键双表

> 创建相关联的表，外键

```mysql
### 省份表为父表，id字段为参照列
### 用户表为子表，pid字段为外键列

# 创建省份表
CREATE TABLE provinces(
	id TINYINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	pname VARCHAR(20) NOT NULL
);

# 创建用户表
CREATE TABLE user1(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(10) NOT NULL,
	sex ENUM('1','2','3') DEFAULT '3',
	phone CHAR(11),
	pid TINYINT UNSIGNED,
	FOREIGN KEY (pid) REFERENCES provinces (id)
);

### 查看索引，如果在命令行中现在按列显示可以加/G
#   如：SHOW INDEXES FROM user/G;
SHOW INDEXES FROM provinces;
SHOW INDEXES FROM user1;
```

> 外键的父表记录被删除可配置好操作
>
> 1. CASCADE：从父表删除或更新且自动删除或更新子表中匹配的行
> 2. SET NULL：从父表删除或更新行，并设置子表中的外键列为NULL。如果使用改选项，必须保证子表列没有指定NOT NULL。
> 3. RESTRICT：拒绝对父表的删除或更新操作。
> 4. NOT ACTION：标准SQL的关键字，在MySQL与RESTRICT相同。

```mysql
# 创建测试用户表user2
CREATE TABLE user2(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(10) NOT NULL,
	sex ENUM('1','2','3') DEFAULT '3',
	phone CHAR(11),
	pid TINYINT UNSIGNED,
	FOREIGN KEY (pid) REFERENCES provinces (id) ON DELETE CASCADE
);

# 插入省份数据
INSERT INTO provinces(pname) VALUES('A');
INSERT INTO provinces(pname) VALUES('B');
INSERT INTO provinces(pname) VALUES('C');

# 查看数据是否正常
SELECT * FROM provinces;

# 插入用户数据
INSERT INTO user2(username,pid) VALUES('Penk',1);
INSERT INTO user2(username,pid) VALUES('Tommy',2);
# 加入无用数据，用于测试，这条报错
# id已经auto_increment，所以user表id会有变化
INSERT INTO user2(username,pid) VALUES('Shawn',4);
INSERT INTO user2(username,pid) VALUES('Jack',3);

# 查看user2记录
SELECT * FROM user2;

# 删除一条记录
DELETE FROM provinces WHERE id=3;

# 查看是否删除成功
SELECT * FROM provinces;

# 查看user2表是否会自动删除
SELECT * FROM user2;
```



# 3、记录级别

### 3.1  单表操作

```mysql
DROP TABLE IF EXISTS user;
# 创建用户表
CREATE TABLE user(
	id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(20) NOT NULL,
	password VARCHAR(20) NOT NULL,
	age TINYINT UNSIGNED NOT NULL DEFAULT 10,
	sex BOOLEAN
);

# 修改密码长度，用户MD5
ALTER TABLE user MODIFY password VARCHAR(32) NOT NULL;

# 插入数据
# 方式一
INSERT INTO user VALUES(NULL,'Penk','111',0,1);
INSERT INTO user VALUES(NULL,'Penk','222',12,1);
INSERT INTO user VALUES(NULL,'Penk','333',0,1);
INSERT INTO user VALUES(NULL,'Penk','444',0,1);
INSERT INTO user VALUES(DEFAULT,'Tommy','222',11,1);
INSERT INTO user VALUES(DEFAULT,'Jack','222',1+2+3+4+5,1);
INSERT INTO user VALUES(DEFAULT,'Shawn',MD5('123'),10,1);
# 方式二
INSERT INTO user SET username='Strive',password='333';
# 方式三
INSERT INTO user (username,password) VALUES('Devin','444');

# 更新数据
# 所有人
UPDATE user set age = age+10;
UPDATE user set age = age+10, sex=0;
UPDATE user set age = age+20, sex=1 WHERE id%2=0;

# 删除记录
DELETE FROM user WHERE id=6;

# 查找记录
SELECT * FROM user;
# 可选择指定列按照指定顺序
SELECT password,username FROM user; 
# 可使用别名
SELECT id AS userId, password AS pwd FROM user;

###
# 根据年龄分组
SELECT username,age FROM user GROUP BY age;
# 根据年龄分组，并且根据年龄排序
SELECT username,age FROM user GROUP BY age ORDER BY age;
# 通过WHERE过滤后再后续操作 
SELECT username,age FROM user WHERE age>30 GROUP BY age ORDER BY age;
# GROUP BY 添加条件判断 HAVING 
SELECT username,age FROM user GROUP BY age HAVING age>30 ORDER BY age;
# 通过LIMIT 限制查询结果返回的数量
# LIMIT {[offset,] row_count}
# offset 为偏移梁，index从0开始，与ID无关，与返回的顺序有关
# row_count 为返回数量
SELECT * FROM user LIMIT 2;
SELECT * FROM user LIMIT 2,2;
SELECT * FROM user ORDER BY id DESC LIMIT 2,2;

SELECT select_expr [,select_expr...]
	[
		FROM table_name
		[WHERE where_condition]
		[GROUP BY {col_name|position}
		[HAVING where_condition]
		[ORDER BY {col_name|expr|positon} [ASC|DESC],...]
		[LIMIT {[offset,] row_count | row_count OFFSET offset}]
	]
```

### 3.2  双表操作

```mysql
### 继续 3.1 操作
# 创建一个临时表，用来存放数据
CREATE TABLE test(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(20)
);

# 删除表里面所有数据，方便测试而已。
DELETE FROM test;
# 插入，user表中想要的数据
INSERT INTO test(username) SELECT username from user GROUP BY username;
# 查询结果
SELECT * FROM test;
```

![image-20200116141722513](MySQL 基本操作.assets/image-20200116141722513.png)

# 4、额外

### 4.1  数据类型

- 整形
- 浮点型
- 字符型
- 日期时间型

https://www.runoob.com/mysql/mysql-data-types.html

### 4.2  约束

> 1. 约束保证数据的完整性和一致性。
> 2. 约束分为表级约束和列级约束。
> 3. 约束类型包括：
>    - NOT NULL
>    - PRIMARY KEY
>    - UNIQUE KEY
>    - DEFAULT
>    - FOREIGN KEY

##### 4.2.1  PRIMARY KEY

- 主键约束

- 主键保证记录的唯一性

- 主键自动设置为NOT NULL

- 每张表只能存在一个主键

##### 4.2.2  UNIQUE KEY

- 唯一约束
- 唯一约束可以保证记录的唯一性
- 唯一约束的字段可以为NULL，但是只能有一个
- 每张数据表可以存在多个唯一约束

##### 4.2.3  DEFAULT

##### 4.2.4  NOT NULL

##### 4.2.5  FOREIGN KEY

- 父表和子表必须使用相同的存储引擎，而且禁止使用临时表。

- 数据表的存储引擎只能为InnoDB。

  > InnoDB，是MySQL的数据库引擎之一，现为MySQL的默认存储引擎，为[MySQL AB](https://baike.baidu.com/item/MySQL AB)发布binary的标准之一。InnoDB由Innobase Oy公司所开发，2006年五月时由[甲骨文公司](https://baike.baidu.com/item/甲骨文公司/430115)并购。与传统的[ISAM](https://baike.baidu.com/item/ISAM)与[MyISAM](https://baike.baidu.com/item/MyISAM)相比，InnoDB的最大特色就是支持了[ACID](https://baike.baidu.com/item/ACID/10738)兼容的[事务](https://baike.baidu.com/item/事务/5945882)（Transaction）功能，类似于[PostgreSQL](https://baike.baidu.com/item/PostgreSQL)。

- 外键列和参照列必须具有相似的数据类型。其中数字的长度或是否有符号位必须相同；而字符的长度则可以不同。

- 外键列与参照列必须创建索引。如果外键列不存在索引的话，MySQL将自动创建索引。

------



> 概念上不同，约束是为了保证数据的完整性，索引是为了辅助查询；
> 创建唯一约束时，会自动的创建唯一索引。
> 关于第二条，MySQL 中唯一约束是通过唯一索引实现的，为了保证没有重复值，在插入新记录时会再检索一遍，怎样检索快，当然是建索引了，所以，在创建唯一约束的时候就创建了唯一索引。

