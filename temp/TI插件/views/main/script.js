// 填写值
async function setValue() {
  let model = $("#model").val() || 'zyp';

  let searchInput = $("input[aria-label='搜索']");
  // 找到就输入指定型号
  searchInput.focus();
  await wait(500);
  searchInput.val(model);
  await wait(500);
  searchInput.blur();
}

// 点击搜索
async function query() {
  document.getElementsByClassName("CoveoSearchButton")[0].click();
}

// input框选择产品并搜索
async function selectProduct(count = 3) {
  console.log(`selectProduct(${$("#model").val()})`);
  await wait(1500);

  if (!count) {
    // 倒数三次，失败则不执行了
    return;
  }

  let searchInput = $("input[aria-label='搜索']");
  // 找不到input则继续找~
  if (searchInput.length == 0) {
    await selectProduct(--count);
    return;
  }

  await setValue();
  await wait(500);

  // 点击搜索按钮
  await query();
}

async function jumpProduct() {
  let product = $("#model").val();
  if ($("#model").val())
    window.location.href = "https://www.ti.com.cn/store/ti/zh/p/product/?p=" + product + "&keyMatch=" + product + "&tisearch=search-everything&usecase=OPN";
}