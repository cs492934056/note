window.addEventListener("load", () => {
  console.log("main load...");
  init();
});

window.addEventListener("DOMContentLoaded", async () => {
  console.log("DOMContentLoaded...");
});

let tiData = {
  model: "tps65217",
  amount: "100",
};

tiData.model = "sn74hc04d";

// 初始化
async function init() {
  // 显示右边浮动框
  initBox();

  // 初始化数据
  initData();

  // 初始化事件
  initMonitor();
}

function initBox() {
  let showBox = document.createElement("div");
  showBox.setAttribute("class", "showBox");
  showBox.setAttribute("draggable", "true");
  document.body.appendChild(showBox);

  let body = [
    "<a class='leftBtn publicBtn'>《</a>" +
    "<a class='rightBtn publicBtn'>》</a>" +
    '</br>搜索型号：<input id="model" value="">',
    '购买数量：<input id="amount" value="">',
    "<a class='oneKeyBtn1 publicBtn'>一键跳转到产品页</a>",
    "------------分割线------------",
    "<a class='oneKeyBtn2 publicBtn'>一键搜索</a>",
    "<a class='setValueBtn publicBtn'>赋值</a>",
    "<a class='queryBtn publicBtn'>搜索</a>",
    "<a class='clearBtn publicBtn'>清空缓存</a>",
  ].join("</br></br>");

  $(".showBox").append(body);
}

function initData() {
  if (localStorageGetItem("tiData")) tiData = localStorageGetItem("tiData");

  if (tiData.model) {
    $("#model").val(tiData.model);
  }

  if (tiData.amount) {
    $("#amount").val(tiData.amount);
  }
}

function initMonitor() {
  // 设置按钮事件
  // 一键跳转到产品页
  $(".oneKeyBtn1").click(jumpProduct);
  // 一键搜索
  $(".oneKeyBtn2").click(selectProduct);
  // 设置值
  $(".setValueBtn").click(setValue);
  // 搜索
  $(".queryBtn").click(query);
  // 清空缓存
  $(".clearBtn").click(() => {
    localStorageDeleteItem("tiData");
  });

  // 监听数据
  // 型号
  $("#model").change((e) => {
    tiData.model = $("#model").val();
    localStorageSetItem("tiData", tiData);
  });

  // 数量
  $("#amount").change((e) => {
    tiData.amount = $("#amount").val();
    localStorageSetItem("tiData", tiData);
  });

  // 显示按钮
  $(".leftBtn").click(() => {
    var obj = document.getElementsByClassName("showBox")[0];
    obj.setAttribute("class", "showBox");
    $(".leftBtn").hide();
    $(".rightBtn").show();
  });

  // 隐藏按钮
  $(".rightBtn").click(() => {
    var obj = document.getElementsByClassName("showBox")[0];
    obj.setAttribute("class", "showBox hiddenBox");
    $(".leftBtn").show();
    $(".rightBtn").hide();
  });
}
