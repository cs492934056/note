window.addEventListener("load", async () => {
  console.log("login load...");
  login();
});

async function login() {
  // 填写账号
  let username = $("input[name='username']");
  await inputValue(username, config.username, 500);

  // 下一步按钮
  let nextButton = $("#nextbutton");
  nextButton.trigger("click");


  // 填写密码
  let password = $("input[name='password']");
  await inputValue(password, config.password, 500);
  password.focus();

  // 登录按钮
  let loginBtn = $("#loginbutton");
  loginBtn.click();
}


