window.addEventListener("load", () => {
  console.log("product load...");
  init();
});

let tiData = {
  amount: "100",
  model: "zyp",
};

function init() {}

// 初始化
async function init() {
  // 显示右边浮动框
  initBox();

  // 初始化数据
  initData();

  // 初始化事件
  initEvent();
}

function initBox() {
  let showBox = document.createElement("div");
  showBox.setAttribute("class", "showBox");
  showBox.setAttribute("draggable", "true");
  document.body.appendChild(showBox);

  let body = [
    "<a class='leftBtn publicBtn'>《</a>" +
      "<a class='rightBtn publicBtn'>》</a>" +
      '</br>搜索型号：<input id="model" value="">',
    '购买数量：<input id="amount" value="">',
    "<a class='oneKeyBtn publicBtn'>一键操作...</a>",
    "------------分割线------------",
    "<a class='setValueBtn publicBtn'>数量赋值</a>",
  ].join("</br></br>");

  $(".showBox").append(body);
}

function initData() {
  if (localStorageGetItem("tiData")) tiData = localStorageGetItem("tiData");

  if (tiData.model) {
    $("#model").val(tiData.model);
  }

  if (tiData.amount) {
    $("#amount").val(tiData.amount);
  }
}

function initEvent() {
  // 设置按钮事件
  // 一键操作
  $(".oneKeyBtn").click(inputAmount);

  // 设置值
  $(".setValueBtn").click(() => {});

  // 数量
  $("#amount").change((e) => {
    tiData.amount = $("#amount").val();
    localStorageSetItem("tiData", tiData);
  });

  // 显示按钮
  $(".leftBtn").click(() => {
    var obj = document.getElementsByClassName("showBox")[0];
    obj.setAttribute("class", "showBox");
    $(".leftBtn").hide();
    $(".rightBtn").show();
  });

  // 隐藏按钮
  $(".rightBtn").click(() => {
    var obj = document.getElementsByClassName("showBox")[0];
    obj.setAttribute("class", "showBox hiddenBox");
    $(".leftBtn").show();
    $(".rightBtn").hide();
  });
}
