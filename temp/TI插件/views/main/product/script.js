function inputAmount() {
  // 获取库存，含有逗号
  let inputAmount = $(".product-note.product-availability")
    .eq(0)
    .find("span")
    .text();

  // 去逗号
  inputAmount = inputAmount.replace(",", "");

  // 数量限制 （有可能不存在）
  let limitAmount = 0;
  let limitBox = document.querySelectorAll("#addToCartForm")[1].querySelector("ti-add-to-cart")
  console.log("111:", limitBox);
  limitBox = limitBox.shadowRoot.querySelector("ti-form-element");
  console.log("222:", limitBox);
  limitBox = limitBox.shadowRoot.querySelector("div.ti-form-element-hint");
  console.log("333:", limitBox);
  if (limitBox&&limitBox.textContent) {
    limitAmount = parseInt(limitBox.textContent.split(": ")[1]);
  }

  // 转成库存整形
  let repertory = limitAmount ? limitAmount : parseInt(inputAmount);
  let amount = parseInt($("#amount").val());
  let targetAmount = repertory > amount ? amount : repertory;

  let table = [
    { name: "库存", value: repertory },
    { name: "购买数量", value: amount },
    { name: "实际采购", value: targetAmount },
  ];

  console.table(table, repertory);

  // TI 采购数量输入框
  let input;

  input = document
    .querySelectorAll("#addToCartForm")[1]
    .querySelector(
      "ti-add-to-cart"
    );
  console.log("111:", input);
  input = input.shadowRoot.querySelector("ti-form-element > ti-input");
  console.log("222:", input);
  input = input.shadowRoot.querySelector("input[type=text]");
  console.log("333:", input);

  input.focus();

  let evt = document.createEvent('HTMLEvents');
  evt.initEvent("input", true, true);

  if (input) {
    input.value = parseInt(targetAmount);
    input.dispatchEvent(evt);
  }

  // 加入购物车按钮
  let btn;

  btn = document
    .querySelectorAll("#addToCartForm")[1]
    .querySelector(
      "ti-add-to-cart"
    );
  console.log("111:", btn);
  btn = btn.shadowRoot.children[1];
  console.log("222:", btn);
  btn = btn.shadowRoot.children[0];
  console.log("333:", btn);

  btn.click();
}
