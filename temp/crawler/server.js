/*
 * @Author: Penk
 * @LastEditors: Penk
 * @LastEditTime: 2021-04-23 14:13:47
 * @FilePath: \test_html\crawler\server.js
 */
var http = require('http');
var fs = require('fs');
var url = require("url");
const querystring = require("querystring");
var req = require('request');

http.createServer(function (request, response) {

  // 发送 HTTP 头部 
  // HTTP 状态值: 200 : OK
  // 内容类型: text/plain
  // response.writeHead(200, {
  //   'Content-Type': 'text/html'
  // });
  var parsedUrl = url.parse(request.url);

  // 判断路由
  // 主路由
  if (parsedUrl.pathname == '/')
    fs.readFile(__dirname + '/index.html', (err, data) => {
      response.end(data)
    })

  // 请求爬虫信息
  if (parsedUrl.pathname == '/getInfo') {
    var tempUrl = querystring.parse(parsedUrl.query).url;
    if (tempUrl) {
      // 特殊字符转义
      tempUrl.replace(/zyp/g, '&');
      crawler(tempUrl, response);
    }
  }



}).listen(8888);

function crawler(url, res) {
  // 通过 GET 请求来读取 http://cnodejs.org/ 的内容
  req(url, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      // 输出网页内容
      // res.writeHead(200, {
      //   'Content-type': 'text/html'
      // });
      // res.write(body);
      // res.end();
      console.log(body);
    }
  });
}

// 终端打印如下信息
console.log('Server running at http://127.0.0.1:8888/');