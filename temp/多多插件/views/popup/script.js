/*
 * @Author: Penk
 * @LastEditors: Penk
 * @LastEditTime: 2021-04-30 15:04:28
 * @FilePath: \多多插件\js\script.js
 */
$("#openBack").click(e => {
  window.open(chrome.extension.getURL("back.html"));
  bg.init();
})

$("#getBackTitle").click(e => {
  var bg = chrome.extension.getBackgroundPage();
  alert(bg.document.title);
})

$("#setBackTitle").click(e => {
  var title = prompt("请输入后台页面新标题", "新标题")
  var bg = chrome.extension.getBackgroundPage();
  bg.document.title = title;
  alert(bg.document.title);
})

$("#onKeyInit").click(e => {
  sendMessageToContentScript({
    cmd: 'init',
    value: '你好，我是popup！'
  }, function (response) {
    console.log('来自content的回复：' + response);
  });
})

function sendMessageToContentScript(message, callback) {
  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, message, function (response) {
      if (callback) callback(response);
    });
  });
}