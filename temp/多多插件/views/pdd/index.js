var showBox, skuBox, input;
var index = 0,
  skuLength,
  originalPrice,
  firstSku,
  longestSkuLength = 0;
// SKU过滤规则
var skuFilter = ["尺寸", "尺码", "大小"];
var pddData = {
  pddKeyWord: "沙發墊",
  initialFreight: 9,
  addHeavyFreight: 2,
  // 需要的倍率
  multipleInput: 5.4,
  priceType: "2",
  lowPriceType: "1",
};
var goods = [];
// 台币汇率
var tbRate = 4.35;
// 商品标题前缀
const titlePrefix = "";
// 商品标题后缀
const titleSuffix = "热销爆款现货";
// 商品详情
var goodsDetails = [];
document.addEventListener("DOMContentLoaded", async () => {
  await wait(1000);
  // 初始化
  await init();

  // 获取并设置商品SKU
  await getSku(index);

  // 获取并设置商品详情
  await getDetail();
});

// isInclude柯理化
function skuFilterFn(val) {
  return isInclude(val, skuFilter);
}

// 延时器
function wait(timeout) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, timeout);
  });
}
// 初始化
async function init() {
  // 出入一个box
  showBox = document.createElement("div");
  showBox.setAttribute("class", "showBox");
  document.body.appendChild(showBox);
  skuLength = $(".r-mksVqr").length;

  $(".showBox").append(
    "<div>" +
    '<button class="leftBtn pddBtn">切换SKU</button>' +
    '<select id="keyWordSelect">' +
    '<option value="Lily-沙發墊">沙發墊</option>' +
    '<option value="Lily-窗簾">窗簾</option>' +
    '<option value="Lily-地墊">地墊</option>' +
    '<option value="Lily-桌墊">桌墊</option>' +
    '<option value="Lily-床上用品">床上用品</option>' +
    '<option value="Lily-浴簾">浴簾</option>' +
    '<option value="Lily-壁紙">壁紙</option>' +
    '<option value="Lily-掛畫">掛畫</option>' +
    '<option value="Lily-門簾">門簾</option>' +
    '<option value="Lily-椅套">椅套</option>' +
    '<option value="Lily-收納盒">收納盒</option>' +
    '<option value="Lily-挂鈎">挂鈎</option>' +
    '<option value="Lily-家具贴纸">家具贴纸</option>' +
    '<option value="Lily-墙壁装饰">墙壁装饰</option>' +
    '<option value="Lily-其他">其他</option>' +
    "</select><br>" +
    '第一个SKU：<span class="firstSku"></span>' +
    '<button class="copyFirstSkuBtn pddBtn" style="margin-left:20px;">拷</button>' +
    '<button class="clearCacheBtn pddBtn" style="margin-left:20px;">清空缓存</button><br>' +
    "</div>" +
    "<div>"
  );

  $(".showBox").append(
    '<form value="运费公式" action="" method="get" style="font-size:16px;"> ' +
    "计费公式<br />" +
    '<label><input name="priceType" type="radio" value="1" check />运费公式</label>  ' +
    '<label><input name="priceType" type="radio" value="2" />原价*价格倍数</label>  ' +
    "</form> " +
    '<form value="低价" action="" method="get" style="font-size:16px;"> ' +
    "低价<br />" +
    '<label><input name="lowPriceType" type="radio" value="1" check />原价</label>  ' +
    '<label><input name="lowPriceType" type="radio" value="2" />折后价</label>  ' +
    "</form> " +
    '<br /><button id="oneKeyPackaging" class="pddBtn" style="display=block;width:100%;">一键打包</button>' +
    '<div class="priceBox">' +
    '原价：<input id="input" class="originPrice">CYN<br>' +
    '转换后未打折价格为：NT$<span class="targetPrice"></span>' +
    '<button class="copyPriceBtn pddBtn" style="margin-left:20px;">拷</button><br>' +
    // 运费模板
    '<div class="transportConfig">' +
    '初始运费：<input id="initialFreight" value="19">CYN<br>' +
    'SKU递增运费：<input id="addHeavyFreight" value="7">CYN<br>' +
    "</div>" +
    // 倍率模板
    '<div class="multipleConfig" style="display:none;">' +
    // '台币汇率：' + tbRate + '<br>' +
    '价格倍数：<input id="multipleInput" value="5.4">CYN<br>' +
    "</div>" +
    "</div><hr />" +
    // 下面为拷贝信息+
    '<div id="descBox" style="margin-top:20px;">' +
    '<span class="otherSizeSpan">【Lily-house北歐居家 #新店活動】北歐ins 專注做北歐風全屋佈置</span>' +
    '<br><br><span class="otherSizeSpan">【廠家自營:支持酒店、民宿 線下北歐風格設計/佈置大批量採購】【全網款式可做，可拿圖訂製，也可聊聊詢問客服拿圖喔】</span>' +
    '<br><span class="otherSizeSpan">💍 关注有礼 关注有礼 关注有礼 关注有礼 💍</span>' +
    '<span class="otherSizeSpan">💍 关注后联系客服查看店铺产品的最新优惠呦 💍</span>' +
    '<br><span class="otherSizeSpan">🌈定製尺寸請聊聊客服詢價下單唷</span>' +
    '<br><span class="otherSizeSpan">🔸🔸🔸🔸🔸🔸🔸🔸🔸🔸</span>' +
    '<br><span class="otherSizeSpan">🔔【超商寄件限制】🔔</span>' +
    '<br><span class="otherSizeSpan">✨超商限制最大體積：45*30*30cm,最重：5kg</span>' +
    '<br><span class="otherSizeSpan">✨超商限制最大體積：45*30*30cm,最重：5kg</span>' +
    '<br><span class="otherSizeSpan">✨不確定如何選擇物流配送方式，請聊聊客服下單唷✨</span>' +
    '<br><span class="otherSizeSpan">🔸🔸🔸🔸🔸🔸🔸🔸🔸🔸</span>' +
    '<br><div class="detailBox"></div>' +
    '<br><span class="otherSizeSpan">【其他現貨尺寸下標指南】:如價格為290，下標先選擇對應花色，再選擇客製尺寸，數量欄輸入29即可！下单请备注尺寸</span>' +
    // sku描述
    '<div class="skuBox"></div>' +
    // 商品描述后缀
    '<br><span class="otherSizeSpan">↓ ↓ ↓ ↓ ↓ ↓ ↓ ░注意░ ↓ ↓ ↓ ↓ ↓ ↓ ↓【貨源地直發】</span>' +
    '<br><span class="otherSizeSpan">如需更多款式尺寸請搜索→→→【Lily-house】</span>' +
    '<br><span class="otherSizeSpan">【現貨】約4～7日出貨</span>' +
    '<br><span class="otherSizeSpan">【預購】約10~15日出貨</span>' +
    '<br><span class="otherSizeSpan">實際到貨時間以貨運狀況為主喔</span>' +
    '<br><span class="otherSizeSpan">親愛的,能下標都是現貨,您可以放心下標哦!</span>' +
    '<br><span class="otherSizeSpan">【溫馨提示】:展示圖片因手機電腦屏幕分辨率，拍攝光線等原因，產品以實際收到貨品為主！凡是以產品有色差，未達到預期效果為由申請退貨，均會拒絕！</span>' +
    '<br><span class="otherSizeSpan">不明白顏色尺寸可以聯絡在線客服哦！</span>' +
    '<br><span class="otherSizeSpan">客服上班時間：早上10:00-晚上10:00，如遇工作期間無回復，親愛的麻煩多發一遍哦，有可能是網絡問題導致信息被蝦皮吃掉啦！</span>' +
    '<br><span class="otherSizeSpan">客製款產品不支持退換貨哦,親愛的下標前請確認是否需要哦!</span>' +
    '<br><span class="otherSizeSpan">✨【Lily-house】讓您安心滿足您的購物慾=</span>' +
    '<br><span class="otherSizeSpan">✔️💥商品品質全數經過層層把關，絕對不販售劣質品</span>' +
    // 关键词
    '<br><span class="otherSizeSpan keyWordSpan">#保暖地墊 #軟墊舒適 #法蘭絨毯 #地墊地毯 #客廳地墊 #北歐地墊 #北歐地墊#廚房踏墊 #門口踏墊 #爬行墊 #居家擺飾 #腳踏墊 #地墊 #地毯  #北歐風</span>' +
    "<div>"
  );

  // 上方填了了DOM才可
  skuBox = document.querySelector("skuBox");

  await wait(1000);

  // 选择相对应的关键字
  $("#keyWordSelect").change((e) => {
    var keyWord = $("#keyWordSelect").children("option:selected").val();

    $(".keyWordSpan").text(keyWordMap.get(keyWord));
    pddData.pddKeyWord = keyWord;
    pddData.pddKeyWordDes = keyWordMap.get(keyWord);
    localStorageSetItem("pddData", pddData);
  });

  // 左切换SKU
  $(".leftBtn").click(() => {
    if (index == 0) {
      index = skuLength - 1;
    } else {
      index--;
    }
    getSku(index);
  });

  $(".copyPriceBtn").click(() => {
    $copyText(originalPrice);
  });

  $(".copyFirstSkuBtn").click(() => {
    $copyText(firstSku);
  });

  // 清空缓存
  $(".clearCacheBtn").click(() => {
    window.localStorage.removeItem("pddData");
    location.reload();
  });

  // 监听价格变化
  $("#input").change(() => {
    getPrice();
  });

  // 监听初始运费
  $("#initialFreight").change(() => {
    pddData.initialFreight = $("#initialFreight").val();
    localStorageSetItem("pddData", pddData);
    getPrice();
    renderDom(goods);
  });

  // 监听SKU递增运费
  $("#addHeavyFreight").change(() => {
    pddData.addHeavyFreight = $("#addHeavyFreight").val();
    localStorageSetItem("pddData", pddData);
    getPrice();
    renderDom(goods);
  });

  // 监听SKU递增运费
  $("#multipleInput").change(() => {
    pddData.multipleInput = $("#multipleInput").val();
    localStorageSetItem("pddData", pddData);
    getPrice();
    renderDom(goods);
  });

  // 监听价格类型
  $("input:radio[name='priceType']").change(function () {
    var priceType = $("input:radio[name='priceType']:checked").val(); //①
    pddData.priceType = priceType;
    localStorageSetItem("pddData", pddData);

    switch (pddData.priceType) {
      // 价格类型为 运费模板
      case "1": {
        $(".transportConfig").css("display", "block");
        $(".multipleConfig").css("display", "none");
        break;
      }
      // 价格类型为 *价格倍数
      case "2": {
        $(".transportConfig").css("display", "none");
        $(".multipleConfig").css("display", "block");
        break;
      }
      default: {
        break;
      }
    }

    renderDom(goods);
  });

  // 监听低价类型
  $("input:radio[name='lowPriceType']").change(function () {
    var lowPriceType = $("input:radio[name='lowPriceType']:checked").val(); //①
    pddData.lowPriceType = lowPriceType;
    localStorageSetItem("pddData", pddData);

    // console.log(pddData.lowPriceType);
    // renderDom(goods);
    getSku(index);
  });

  // 一键打包
  $("#oneKeyPackaging").click((e) => {
    let title =
      $(".enable-select").text() == ""
        ? $("._1fdrZL9O").text()
        : $(".enable-select").text();
    let data = {
      /* 第一个SKU，用于设置最小尺寸 */
      firstSku,
      /* 第一个SKU对应的价格，用于统一价格 */
      originalPrice,
      /* 商品标题 */
      goodsTitle: titlePrefix + title + titleSuffix,
      /* 设置商品分类 */
      firstClassify: $("#keyWordSelect").children("option:selected").val(),
      /* 价格类型 1 试用计费模板 2 价格*价格倍数 */
      priceType: pddData.priceType,
      lowPriceType: pddData.lowPriceType,
      /**台币汇率 */
      pddKeyWordDes: pddData.pddKeyWordDes,
      goodsDetails,
    };

    switch (pddData.priceType) {
      case "1": {
        break;
      }
      case "2": {
        data.originalPrice = pddData.multipleInput;
        break;
      }
      default:
        break;
    }

    /* 商品规格数量 */
    data.skuKindNum = skuLength;
    /* 需要过滤的商品规格，有2个才设置，会将其对应的初始化 */
    data.filterSkuKind = "";

    if (skuLength > 1) {
      data.filterSkuKind = $(".r-mksVqr:eq(" + index + ")")
        .children("span")
        .text();
    }

    /* 需要复制的内容的 */
    const range = document.createRange();
    range.selectNode(document.getElementById("descBox"));
    const selection = window.getSelection();
    if (selection.rangeCount > 0) selection.removeAllRanges();
    selection.addRange(range);
    data.goodsDesc = window.getSelection().toString();

    data.goods = goods;

    let str = JSON.stringify(data);
    $copyText(str);
  });

  Array.from($(".r-mksVqr .sku-specs-key")).forEach((e, index1) => {
    if (skuFilterFn(e.textContent)) {
      index = index1;
    }
  });

  // 初始化本地数据
  // 设置默认值SKU
  if (localStorageGetItem("pddData")) {
    pddData = localStorageGetItem("pddData");
    // 读取存在local的关键字
    if (pddData.pddKeyWord) {
      // 触发
      $("#keyWordSelect").val(pddData.pddKeyWord);
      $("#keyWordSelect").trigger("change");
    }

    if (pddData.initialFreight) {
      $("#initialFreight").val(pddData.initialFreight);
    }

    if (pddData.addHeavyFreight) {
      $("#addHeavyFreight").val(pddData.addHeavyFreight);
    }

    if (pddData.multipleInput) {
      $("#multipleInput").val(pddData.multipleInput);
    }

    if (pddData.priceType) {
      $(`input:radio[name='priceType'][value=${pddData.priceType}]`).attr(
        "checked",
        true
      );
    } else {
      $(`input:radio[name='priceType'][value=1]`).attr("checked", true);
    }

    if (pddData.priceType) {
      $(`input:radio[name='lowPriceType'][value=${pddData.lowPriceType}]`).attr(
        "checked",
        true
      );
    } else {
      $(`input:radio[name='lowPriceType'][value=1]`).attr("checked", true);
    }

    switch (pddData.priceType) {
      // 价格类型为 运费模板
      case "1": {
        $(".transportConfig").css("display", "block");
        $(".multipleConfig").css("display", "none");
        break;
      }
      // 价格类型为 *价格倍数
      case "2": {
        $(".transportConfig").css("display", "none");
        $(".multipleConfig").css("display", "block");
        break;
      }
      default: {
        break;
      }
    }
  } else {
    // // 填写默认参数
    $(`input:radio[name='lowPriceType'][value=${pddData.lowPriceType}]`).attr(
      "checked",
      true
    );
    $(`input:radio[name='priceType'][value=${pddData.priceType}]`).attr(
      "checked",
      true
    );

    switch (pddData.priceType) {
      // 价格类型为 运费模板
      case "1": {
        $(".transportConfig").css("display", "block");
        $(".multipleConfig").css("display", "none");
        break;
      }
      // 价格类型为 *价格倍数
      case "2": {
        $(".transportConfig").css("display", "none");
        $(".multipleConfig").css("display", "block");
        break;
      }
      default: {
        break;
      }
    }
  }
}

// 算法计算公式
function sum(val, i = 0) {
  let initialFreight = 19,
    addHeavyFreight = 7;

  initialFreight = parseFloat($("#initialFreight").val());
  addHeavyFreight = parseFloat($("#addHeavyFreight").val());

  return Math.ceil(
    (parseFloat(val) + initialFreight + addHeavyFreight * i) * 1.54 * 4.5 * 4
  );
}

// 根据input换算出价格
function getPrice() {
  let val = $("#input").val();
  switch (pddData.priceType) {
    // 价格类型为 运费模板
    case "1": {
      originalPrice = sum(val);
      break;
    }
    // 价格类型为 *价格倍数
    case "2": {
      originalPrice = Math.ceil(val * 5 * 4.5);
      break;
    }
    default: {
      console.log("getPrice() error!");
      break;
    }
  }
  $(".targetPrice").text(originalPrice);
}

// 获取商品详情
function getDetail() {
  var detail;
  for (let i = 0; i < $("._8rUS_gSm").length; i++) {
    detail =
      $("._8rUS_gSm ._1M3pVo3W:eq(" + i + ")").text() +
      "：" +
      $("._8rUS_gSm ._32_tX1hK:eq(" + i + ")").text();

    if (detail.includes("品牌")) continue;

    var br = document.createElement("br");
    var textNode = document.createTextNode(detail);

    goodsDetails.push(detail);
    $(".detailBox").append(textNode);
    $(".detailBox").append(br);
  }
}

// 获取当前商品的SKU，如果是样式加尺寸则获取SKU
function getSku(index) {
  goods = [];

  // 判断是否有库存，没有则下一个
  function selectFirstItem(btn) {
    if (btn[0].outerHTML.includes("sku-spec-value-disable")) {
      selectFirstItem(btn.next());
    }
    // 判断有无选择，没选择就点击
    else if (!btn[0].outerHTML.includes("sku-spec-value-selected") && !btn[0].outerHTML.includes("_12nPsLEr")) {
      btn.click();
    }
  }

  // 所有的SKU选中第一个
  for (var i = 0; i < skuLength; i++) {
    let firstSKU = $(".r-mksVqr:eq(" + i + ")")
      .children(".qK4302ba")
      .children("div:first-child");

    selectFirstItem(firstSKU);
  }

  // 主SKU
  var mainSku = $(".qK4302ba:eq(" + index + ")");

  // if (!$(element).attr("class").includes("_12nPsLEr"))
  // 遍历SKU
  Array.from(mainSku.children()).forEach((element, index1) => {
    if ($(element).attr("class").includes("sku-spec-value-disable")) return;

    element.click();
    let val = $("._9AczZsFY ._27FaiT3N").text();

    if (val.slice(1).includes("-")) {
      element.click();
      val = $("._9AczZsFY ._27FaiT3N").text();
    }

    if (pddData.lowPriceType == "2" && $("._9AczZsFY .mEK_vXG0").length > 0) {
      val = $("._9AczZsFY .mEK_vXG0").text();
    }

    goods.push({
      sku: element.textContent.replace(/ /g, ""),
      originPrice: val.split("¥")[1],
    });

    if (strlen(goods[goods.length - 1].sku) > longestSkuLength) {
      longestSkuLength = strlen(goods[goods.length - 1].sku);
    }
  });

  // 价格排序
  goods.sort((a, b) => {
    return a.originPrice - b.originPrice;
  });

  // 去除定制SKU
  goods = goods.filter((e) => {
    let flag =
      !e.sku.includes("定制") &&
      !e.sku.includes("尺寸") &&
      !e.sku.includes("客服") &&
      !e.sku.includes("备注") &&
      !e.sku.includes("定做");
    return flag;
  });

  renderDom(goods);
}

// 渲染DOM
function renderDom(goods) {
  // 清空SKU显示列表
  $(".skuBox").empty();

  $(".skuBox").append(
    `<br><span class="otherSizeSpan">【商品尺寸、价格】</span>`
  );

  // 梯度增加价格，避免快递费问题
  goods.forEach((element, index1) => {
    switch (pddData.priceType) {
      // 价格类型为 运费模板
      case "1": {
        element.targetPrice = Math.ceil(sum(element.originPrice, index1) / 4);
        break;
      }
      // 价格类型为 *价格倍数
      case "2": {
        element.targetPrice = (
          (element.originPrice * pddData.multipleInput * tbRate) /
          2
        ).toFixed(2);
        break;
      }
      default: {
        element.targetPrice = "haha";
        break;
      }
    }
  });

  // 渲染在DOM文档对象模型中
  goods.forEach((e, index) => {
    let addSpaceNum = longestSkuLength - strlen(e.sku);
    let str = "";
    for (var i = 0; i < addSpaceNum; i++) {
      str = str + "\u00A0";
    }
    $(".skuBox").append(
      `<span>${e.sku} \u00A0\u00A0 ${str} ${e.targetPrice}元</span><br>`
    );
  });

  // 获取价格
  $("#input").val(goods[0].originPrice);
  getPrice();

  // 设置第一个SKU
  $(".firstSku").text(goods[0].sku);
  firstSku = goods[0].sku;
}
