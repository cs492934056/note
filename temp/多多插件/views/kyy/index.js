/*
 * @Author: Penk
 * @LastEditors: Penk
 * @LastEditTime: 2021-10-18 19:18:34
 * @FilePath: \多多插件\views\kyy\index.js
 */
window.addEventListener("load", async () => {
  // $$ = jQuery.noConflict();
  // 右上角按钮框
  initBox();

  if (localStorageGetItem("kyy")) {
    kyy = localStorageGetItem("kyy");
  } else {
    localStorageSetItem("kyy", kyy);
  }

  let date = new Date();
  let dateStr = "" + date.getFullYear();
  dateStr += "" + (date.getMonth() + 1);
  dateStr += "" + date.getDate();

  // 判断客优云存储的时间，主要是汇率变化
  if (kyy.dateStr != dateStr) {
    kyy.tbRate = null;
  }

  // 获取存储的汇率
  $("#tbRate").val(kyy.tbRate);

});

//监听刷新页面事件方法
window.onbeforeunload = function (event) {
};

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  // console.log(sender.tab ?"from a content script:" + sender.tab.url :"from the extension");
  if (request.cmd == "initCollect") {
    initCollect();
    sendResponse("我收到了你的消息！");
  }
});




// 批量编辑
async function bulkEdit(timeout = 1000) {
  anchors("定价工具");
  return new Promise(async (resolve, reject) => {

    console.log(222);
    // 小数处理方式
    switch (pddData.priceType) {
      // 价格类型为运费模板
      case "1": {
        selectItem("取整");
        break;
      }
      // 价格5倍
      case "2": {
        selectItem("保留两位小数");
        break;
      }
      default: {
        console.log("设置折扣值:数据有问题~");
        break;
      }
    }

    // 规格编码
    await inputValue($('input[aria-label="规格编码"]')[0], timestamp);

    // 价格
    var originalPriceInput = $('div[title="批量编辑"]')
      .parent()
      .find('input[aria-label="价格"]')[0];
    await inputValue(originalPriceInput, pddData.originalPrice);

    // 库存
    var stockInput = $('div[title="批量编辑"]')
      .parent()
      .find('input[aria-label="库存"]')[0];
    await inputValue(stockInput, stock);

    await wait(1000);

    // 判断价格模板类型
    switch (pddData.priceType) {
      // 价格类型为运费模板
      // 由于是统一价格，一键编辑再单独价格/5
      case "1": {
        // 价格计算方式
        selectItem("统一值");

        await wait(1000);
        // 价格 / 5
        await setOriginPrice();

        break;
      }
      // 价格5倍
      // 由于是不统一价格，所以是先设置通用价格再一键编辑
      case "2": {
        // 价格计算方式
        selectItem("相乘");

        await wait(1000);
        // 先设置通用价格
        await initGeneralPrice();

        break;
      }
      default: {
        console.log("批量编辑:bulkEdit,数据有问题~");
        break;
      }
    }

    // 全选了才能操作
    // 打开全部check
    let allCheck = $('div[title="商品规格列表"]').next().find("th").eq(0).find("input")[0];
    allCheck.click();
    await wait(500);
    allCheck.click();
    await wait(500);
    for (let i = 0; i < 3; i++) {
      if (!allCheck.checked) {
        allCheck.click();
        await wait(500);
      }
    }

    await wait(1500);

    // 批量编辑一键设置按钮
    var btn = getHandleBySelectorAndText(
      $(".v-btn__content"),
      "一键设置",
      true
    );
    btn[0].click();
    await wait(timeout);

    resolve();
  });
}

async function oneKeyDiscount(discount = false) {
  await wait(500);

  // 跳转
  anchors("定价工具");
  await wait(800);

  // 设置最低价格
  // await setTargetPrice();
  // await wait(1200);

  // 翻译
  $(".bottom-panel button").eq(0).click();
  getHandleBySelectorAndText($(".v-list__tile__title"), "中文(简)->繁").trigger(
    "click"
  );
  await wait(500);

  // 点击折扣活动
  $('input[aria-label="折扣活动"]').click();
  await wait(500);

  // 选择折扣
  $('.v-input--selection-controls__input input[role="radio"]').eq(0).click();
  await wait(200);

  // 点击确定按钮
  $(".v-dialog--scrollable .v-btn__content").eq(3).click();

  if (discount) {
    await inputValue($('input[aria-label="折扣"]')[1], discount);
  }
  // 设置折扣值
  // 判断价格模板类型
  else
    switch (pddData.priceType) {
      // 价格类型为运费模板
      case "1": {
        await inputValue(
          $('input[aria-label="折扣"]')[1],
          discount ? discount : 75
        );
        break;
      }
      // 价格5倍
      case "2": {
        await inputValue(
          $('input[aria-label="折扣"]')[1],
          discount ? discount : 50
        );
        break;
      }
      default: {
        console.log("设置折扣值:数据有问题~");
        break;
      }
    }
  await wait(500);

  // 打开折扣按钮
  let disLen = $('div[title="商品规格列表"]').next().find("th").length;
  let discountRadio;
  for (let i = 0; i < disLen; i++) {
    var text = $('div[title="商品规格列表"]').next().find("th").eq(i).text();
    if (text == '折扣') {
      discountRadio = $('div[title="商品规格列表"]').next().find("th").eq(i).find("input")
      break;
    }
  }
  discountRadio.click();
  await wait(500);

  // 全选了才能操作
  // 重新全部check
  let allCheck = $('div[title="商品规格列表"]').next().find("th").eq(0).find("input")[0];
  allCheck.click();
  await wait(500);
  allCheck.click();
  await wait(500);

  // 折扣设置一键设置
  $('div[title="折扣设置"]').parent().find(".v-btn__content").eq(0).click();
  await wait(500);

  // 取整按钮
  var floorBtn = getHandleBySelectorAndText(
    $(".v-menu__content .v-list__tile__title"),
    "取整"
  );
  floorBtn.click();
  await wait(500);
}