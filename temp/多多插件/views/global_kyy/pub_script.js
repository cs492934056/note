// 导入基础数据
// 是否自动初始化
var autoPlay = false;
// SKU过滤规则
var skuFilter = ["尺寸", "尺码", "大小"];
var skuKindNum = 0;
// 商品标题前缀
var goodsPrefix = "免运极速发货厂家直销";
// 商品标题后缀
var goodsSuffix = "热销爆款现货北欧简约";
// 库存
var stock = "1000";
// 商品重量
var spzl = 0.01;
// 商品尺寸
var spcc = [1, 1, 1];
// 物流方式
var wlfs = [
  "蝦皮海外 - 宅配 (max 20kg)",
  // "蝦皮海外 - 7-11 (max 10kg)",
  // "蝦皮海外 - 全家 (max 10kg)",
  // "蝦皮海外 - 萊爾富（空運） (max 10kg)",
];
var timestamp = new Date().getTime();
var $$;

// 通用SKU
var generalSku = "其他尺寸请看描述价格下单";

// 是否在运行中
var isRunning = false;

// 次要SUK index  有通用SKU种类
var subSkuIndex;

var kyy;


// 下拉框选择按钮
function selectItem(itemName, timeout = 500) {
  return new Promise((resolve, reject) => {
    //如果选项有重复项需小心~~~~~！！！！
    Array.from($("#appRoot .v-select-list .v-list__tile__title")).forEach(
      (item) => {
        if (item.textContent == itemName) {
          item.click();
          setTimeout(() => {
            resolve();
            return;
          }, timeout);
        }
      }
    );

    Array.from($("#appRoot .v-cascader-list .v-list__tile__title")).forEach(
      (item) => {
        if (item.textContent.includes(itemName)) {
          item.click();
          setTimeout(() => {
            resolve();
            return;
          }, timeout);
        }
      }
    );

    setTimeout(() => {
      resolve();
      return;
    }, timeout);
  });
}

// 修改input标签
function updateGoodsCode() {
  let handle = $('#appRoot div[title="全球商品货号"]');

  return new Promise((resolve, reject) => {
    // 找到句柄
    handle = handle.next();

    handle.find(".v-input__control").click(async (e) => {
      await wait(500);

      // 点击后出来input才能执行，需要放在timeout中
      var inputHandle =
        handle.find("textarea").length > 0
          ? handle.find("textarea")[0]
          : handle.find("input")[0];
      await inputValue(inputHandle, timestamp);
      // 清除事件
      handle.find(".v-input__control").unbind(e);
      resolve();
    });

    // 手动触发
    handle.find(".v-input__control")[0].click();
  });
}
// 设置input值
function inputValue(el, val) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      let evt = document.createEvent("HTMLEvents");
      evt.initEvent("input", true, true);
      el.value = val;
      el.dispatchEvent(evt);
      resolve();
    }, 500);
  });
}

// 设置checkBox
function checkBox(el, attr, val) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (attr == val + "") {
        el.click();
      }
      resolve();
    }, 50);
  });
}

function getHandleBySelectorAndText(selector, text, isArray = false) {
  let handle = typeof selector == "String" ? $(selector) : selector;
  let len = handle.length;
  let temp;
  let arr = [];

  if (isArray) {
    for (var i = 0; i < len; i++) {
      temp = handle.eq(i).text();
      if (temp.includes(text)) {
        arr.push(handle.eq(i));
      }
    }
    return arr;
  } else
    for (var i = 0; i < len; i++) {
      temp = handle.eq(i).text();
      if (temp.includes(text)) {
        return handle.eq(i);
      }
    }
}

// 遍历锚点，处理懒加载
async function anchors(target, timeout = 500) {
  var position = 0;
  if (!target) {
    for (let i = 0; i < $('p.info-section__title').length; i++) {
      position = $('p.info-section__title').eq(i).offset();
      position.top = position.top - 60;
      $("html,body").animate({ scrollTop: position.top }, timeout);
      await wait(timeout);
    }

    return;
  }

  let temp = $(`p.info-section__title:contains(${target})`);
  if (temp.length == 0) {
    temp = $(`div.v-form-item__label:contains(${target})`)
  }

  if (temp.length > 0) {
    position = temp.offset();
    position.top = position.top - 60;
    $("html,body").animate({ scrollTop: position.top }, timeout);
  }
}
