/*
 * @Author: Penk
 * @LastEditors: Penk
 * @LastEditTime: 2021-10-18 20:52:50
 * @FilePath: \多多插件\views\global_kyy\collect\index.js
 */
window.addEventListener("load", async () => {
  // $$ = jQuery.noConflict();
  // 右上角按钮框
  initBox();

  if (localStorageGetItem("kyy")) {
    kyy = localStorageGetItem("kyy");
  } else {
    localStorageSetItem("kyy", kyy);
  }
});

//监听刷新页面事件方法
window.onbeforeunload = function (event) {
};

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  // console.log(sender.tab ?"from a content script:" + sender.tab.url :"from the extension");
  if (request.cmd == "initCollect") {
  }
});

// 客优云弄个固定窗口存放按钮
async function initBox() {
  var kyyBox = document.createElement("div");
  kyyBox.setAttribute("class", "kyyBox");
  document.body.appendChild(kyyBox);

  $(".kyyBox").append(
    '<div class="kkyContent">' +
    "<br>" +
    '<textarea rows="5" class="textarea">请替换拼多多一键打包的数据，采集页面才需要~~~</textarea>' +
    "<br>" +
    '<button class="collectBtn pddBtn">一键采集</button>' +
    "<br>" +
    "------------辅助功能------------" +
    "<br>" +
    '<button class="initBtn pddBtn">一键初始化</button>' +
    // "<br>" +
    // '<button class="refreshBtn pddBtn">刷新库存</button>' +
    // '<button class="clearBtn pddBtn">清空库存</button>' +
    // "<br>" +
    '<button class="testBtn pddBtn">test</button>' +
    "<br>" +
    // '打折数：<input id="discountInput" value="50" />' +
    // '<button class="goodsBtn pddBtn">一键折扣</button>' +
    // "<br>" +
    '物流天数：<input id="expressInput" value="5"></input>' +
    '<button class="expressBtn pddBtn">初始化物流</button>' +
    "</div>"
  );

  await wait(500);
  $(".textarea").bind("input propertychange", () => {
    let data = JSON.parse($(".textarea").eq(0).val());
    pddData.firstSku = data.firstSku;
    pddData.originalPrice = data.originalPrice;

    pddData.goodsTitle = data.goodsTitle;
    pddData.skuKindNum = data.skuKindNum;
    pddData.firstClassify = data.firstClassify;
    pddData.filterSkuKind = data.filterSkuKind;
    pddData.goods = data.goods;
    pddData.priceType = data.priceType;
    pddData.pddKeyWordDes = data.pddKeyWordDes;
    pddData.goodsDetails = data.goodsDetails;
    pddData.goodsDesc = data.goodsDesc;

    skuFilter.push(data.filterSkuKind);

    // 避免复制上上次的数据
    $copyText("请先拷贝拼多多一键打包数据，粘贴一次即可~~~");
  });

  // 一键采集按钮
  $(".collectBtn").click(async () => {

    if (
      $(".textarea").eq(0).val() ==
      "请替换拼多多一键打包的数据，采集页面才需要~~~"
    ) {
      console.log("请先粘贴拼多多数据...");
      return;
    }

    if (isRunning) {
      console.log("isRunning...");
      return;
    }

    isRunning = true;

    await wait(1000);

    // 遍历锚点，处理懒加载
    await anchors();
    await wait(2000);

    await initCollect();

    isRunning = false;
  });

  // 一键折扣
  $(".goodsBtn").click(async () => {
    if (isRunning) {
      console.log("isRunning...");
      return;
    }

    isRunning = true;

    await oneKeyDiscount($("#discountInput").val());

    // 自动更新
    getHandleBySelectorAndText($(".v-btn__content"), "更新").click();

    isRunning = false;
  });

  // 普通初始化
  $(".initBtn").click(async () => {
    if (isRunning) {
      console.log("isRunning...");
      return;
    }

    isRunning = true;

    await wait(1000);

    initKyy();

    // 自动更新

    isRunning = false;
  });

  // 清空库存
  $(".clearBtn").click(async () => {
    if (isRunning) {
      console.log("isRunning...");
      return;
    }

    isRunning = true;

    anchors("批量编辑");
    await wait(1000);

    // 库存
    var stockInput = $('div[title="批量编辑"]')
      .parent()
      .find('input[aria-label="库存"]')[0];
    await inputValue(stockInput, 0);

    // 批量编辑一键设置按钮
    var bulkBtn = getHandleBySelectorAndText(
      $(".v-btn__content"),
      "一键设置",
      true
    );
    bulkBtn[0].click();

    await wait(500);

    $(".anchors span").eq(5).trigger("click");
    await wait(200);

    // 点击折扣活动
    $('input[aria-label="折扣活动"]').click();
    await wait(500);

    // 点击确定按钮
    $("#inspire .v-btn__content")[3].click();
    await wait(1000);

    $("#priceAndStock div.v-chip__close").click();

    await wait(500);
    // 自动更新
    getHandleBySelectorAndText($(".v-btn__content"), "更新").click();

    isRunning = false;
  });

  // test
  $(".testBtn").click(async () => {
    if (isRunning) {
      console.log("isRunning...");
      return;
    }

    isRunning = true;

    await test();

    isRunning = false;
  });

  // 刷新库存
  $(".refreshBtn").click(async () => {
    if (isRunning) {
      console.log("isRunning...");
      return;
    }

    isRunning = true;

    // 时间间隔作为每日更新模板
    var s1 = "2021-05-27";
    s1 = new Date(s1.replace(/-/g, "/"));
    //当前日期
    s2 = new Date();
    var days = s2.getTime() - s1.getTime();
    var time = parseInt(days / (1000 * 60 * 60 * 24));

    anchors("批量编辑");
    await wait(1000);

    // 库存
    var stockInput = $('div[title="批量编辑"]')
      .parent()
      .find('input[aria-label="库存"]')[0];
    await inputValue(stockInput, parseInt(stock) + time);

    // 批量编辑一键设置按钮
    var bulkBtn = getHandleBySelectorAndText(
      $(".v-btn__content"),
      "一键设置",
      true
    );
    bulkBtn[0].click();

    await wait(500);

    // 自动更新
    getHandleBySelectorAndText($(".v-btn__content"), "更新").click();
    isRunning = false;
  });

  // 初始化物流
  $(".expressBtn").click(async () => {
    if (isRunning) {
      console.log("isRunning...");
      return;
    }

    isRunning = true;

    anchors("物流信息");
    await wait(1000);

    let val = $("#expressInput").val();
    if (!val) {
      console.log("请先填写发货期限，默认5...");
      return;
    }

    // 物流信息
    await expressInfo();

    isRunning = false;
  });

  // 监听运货期
  $("#expressInput").change((inputObj) => {
    let val = $("#expressInput").val();
    if (!/(^0$)|(^100$)|(^\d{1,2}$)/.test(val)) {
      val = parseInt(val);
      if (val > 30) {
        val = 30;
      }
      if (val < 3) {
        val = 3;
      }
    }
    val = parseInt(val);
    if (val > 30) {
      val = 30;
    }
    if (val < 3) {
      val = 3;
    }
    $("#expressInput").val(val);
  });
}

async function initKyy() {
  anchors("店铺和类目");
  // 选择適用空間
  await selectItem("臥室(卧室)");

  // 选择風格
  await selectItem("歐式(欧式)");

  // 选择材质
  await selectItem("混紡(混纺)");

  // 选择品牌
  await selectItem("自有品牌(自有品牌)");

  // 添加商品编码
  await updateGoodsCode();
  // 规格编码
  await inputValue($('input[aria-label="规格编码"]')[0], timestamp);

  anchors("物流信息");
  // 物流信息
  await expressInfo();
}

async function initCollect() {

  anchors("店铺和类目");

  await selectItem("朝阳区颜勇商贸行");

  anchors("商品信息");

  // 初始化类目
  await wait(2000);
  // 自动同步了类目~
  await initShopCategory();

  // 选择客优云分类
  // 暂时没有了分类
  // await selectItem(pddData.firstClassify);

  // 修改商品title
  await addGoodsSuffix();

  // 添加商品编码
  await updateGoodsCode();

  // 添加商品描述
  // await addGoodsDesc();

  anchors("价格库存");

  // 需要过滤才初始化SKU以及设置价格表
  // 初始化SKU
  await initSku();

  // 批量编辑
  // await wait(500);
  await bulkEdit();


  await wait(500);
  // await oneKeyDiscount();

  // 物流信息
  await wait(500);
  await expressInfo();

  // 保存pdd数据，到全球商品可复用
  localStorageSetItem("kyyData", {
    pddData,
    globalGoodsCode: timestamp
  });

  // 默认发布，没什么可以看的
  // 主要是看详情中的商品价格对的上不
  // 可在全球商品编辑再次更改
  // 批量编辑一键设置按钮

  var fbBtn = getHandleBySelectorAndText(
    $(".v-btn__content"),
    " 发布 ",
    true
  );
  fbBtn[0].click();
}

async function test() {
  // await selectItem(" Home & Living (家居和生活) ", 500);
  // await selectItem(" Decoration (装修) ", 500);
  // await selectItem(" Furniture & Appliance Covers (家具和家电覆盖) ", 2000);
  // await selectItem("NoBrand(NoBrand)");
  // await selectItem("其他(其他)");
  // await selectItem("否(否)");


  let allCheck = $('div[title="商品规格列表"]').next().find("th").eq(0).find("input");
  allCheck[0].setAttribute("id", "allCheck");

  console.log("hello~!");
}
