
console.log('test');

window.addEventListener('load', () => {
  // 初始化
  console.log('init');
  init();
})

function init() {
  // let t = document.getElementsByClassName('c_l_area c_a_n')[0];
  // let evt = document.createEvent('HTMLEvents');
  // evt.initEvent('input', true, true);
  // t.value = 'setValue';
  // t.dispatchEvent(evt)

  // var inputElement = document.getElementsByClassName("checkInput")[0];
  // // inputElement.value = '123';

  // input.addEventListener('input', ()=>{});

  setTimeout(() => {
    // $('iput.checkInput').val('haha');


    let t = document.getElementsByClassName('checkInput')[0];
    let evt = document.createEvent('HTMLEvents');
    evt.initEvent('input', true, true);
    t.value = 'setValue';
    t.dispatchEvent(evt);
  }, 1000);
}


function fireKeyEvent(el, evtType, keyCode) {
  var evtObj;
  if (document.createEvent) {
    if (window.KeyEvent) {//firefox 浏览器下模拟事件
      evtObj = document.createEvent('KeyEvents');
      evtObj.initKeyEvent(evtType, true, true, window, true, false, false, false, keyCode, 0);
    } else {//chrome 浏览器下模拟事件
      evtObj = document.createEvent('UIEvents');
      evtObj.initUIEvent(evtType, true, true, window, 1);

      delete evtObj.keyCode;
      if (typeof evtObj.keyCode === "undefined") {//为了模拟keycode
        Object.defineProperty(evtObj, "keyCode", { value: keyCode });
      } else {
        evtObj.key = String.fromCharCode(keyCode);
      }

      if (typeof evtObj.ctrlKey === 'undefined') {//为了模拟ctrl键
        Object.defineProperty(evtObj, "ctrlKey", { value: true });
      } else {
        evtObj.ctrlKey = true;
      }
    }
    el.dispatchEvent(evtObj);

  } else if (document.createEventObject) {//IE 浏览器下模拟事件
    evtObj = document.createEventObject();
    evtObj.keyCode = keyCode
    el.fireEvent('on' + evtType, evtObj);
  }
}