/*
 * @Author: Penk
 * @LastEditors: Penk
 * @LastEditTime: 2021-10-29 16:28:16
 * @FilePath: \多多插件\config\config.js
 */

var needEmoji = false;

// 商品标题前缀
var goodsPrefix = "免运";
// 商品标题后缀
var goodsSuffix = "厂家直銷热销爆款现货";

var dlData = {
  prefix: [
    "【Lily-house 北欧居家*新店活动】ins专注做北欧风全屋n\n",
    "【厂家自营：支持酒店、民宿回收风格风格 / 寄大线喔精品网采购】【全可做，可拿图订制，也聊聊咨询客服拿图】",
    "定制尺寸请聊聊客服下单唷\n",
    "【超商寄件限制】",
    "《超商限制最大体积：45 * 30 * 30cm，最重：5kg》",
    "《超商限制最大体积：45 * 30 * 30cm，最重：5kg》",
    "到底如何选择物流传送方式，请聊聊客服下唷",
  ].join("\n"),
  suffix: [
    "\n↓↓↓↓↓↓↓↓↓░注意░↓↓↓↓↓↓↓↓【货源地直】】",
    "如需更多图片尺寸请询问客服→→→",
    "\n【鸳】约4～7日出国",
    "【预购约10~15日出国】",
    "\n实际到货时间以货运状况为主喔",
    "亲爱的，能下标都是屁股，你可以放心下标哦！",
    "\ n【温馨提示】：！展示图片因手机电脑屏幕分辨率，拍摄光线等原因，产品以收到货品为主凡是以产品有色差，达到达到效果为由申请退货均均拒绝",
    "不明白颜色尺寸可以联系在线客服哦！",
    "客服工作时间：早上10:00-晚上10:00，如遇工作期间无回复亲爱的，有麻烦被多发了，有可能是网络问题信息，虾皮吃掉啦！",
    "客制款产品不支持退换货哦，亲爱的下标前请确认是否哦！",
    "让您安心满足您的购物欲",
    "商品质量全经过层层把关，绝对不卖劣质品",
  ].join("\n"),
};

var tempData = {
  prefix: [
    "【Lily-house北欧居家 *新店活动】北欧ins 专注做北欧风全屋布置\n",
    "【厂家自营:支持酒店、民宿 线下北欧风格设计/布置大批量采购】【全网款式可做，可拿图订制，也可聊聊询问客服拿图喔】",
    "🌈定制尺寸请聊聊客服询价下单唷\n",
    "🔸🔸🔸🔸🔸🔸🔸🔸🔸🔸",
    "🔔【超商寄件限制】🔔",
    "✨超商限制最大体积：45*30*30cm,最重：5kg",
    "✨超商限制最大体积：45*30*30cm,最重：5kg",
    "✨不确定如何选择物流配送方式，请聊聊客服下单唷✨",
    // "🔸🔸🔸🔸🔸🔸🔸🔸🔸🔸\n",
  ].join("\n"),
  suffix: [
    "\n↓ ↓ ↓ ↓ ↓ ↓ ↓ ░注意░ ↓ ↓ ↓ ↓ ↓ ↓ ↓【货源地直发】",
    "如需更多款式尺寸请搜索→→→【Lily-house】",
    "\n【现货】约4～7日出货",
    "【预购】约10~15日出货",
    "\n实际到货時間以货运状况为主喔",
    "亲爱的,能下標都是现货,您可以放心下標哦!",
    "\n【溫馨提示】:展示圖片因手機電腦屏幕分辨率，拍攝光線等原因，產品以实际收到货品为主！凡是以產品有色差，未達到預期效果为由申请退货，均會拒絕！",
    "不明白顏色尺寸可以聯絡在線客服哦！",
    "客服上班時間：早上10:00-晚上10:00，如遇工作期間無回復，亲爱的麻煩多發一遍哦，有可能是網絡問題導致信息被蝦皮吃掉啦！",
    "客製款產品不支持退換货哦,亲爱的下標前请確認是否需要哦!",
    "\n✨【Lily-house】讓您安心滿足您的購物慾",
    "✔️💥商品品質全數經過層層把關，絕對不販售劣質品",
  ].join("\n"),
};

if (needEmoji) {
  dlData = tempData;
}

var keyWordMap = new Map();
// 关键字优化
keyWordMap.set(
  "Lily-掛畫",
  "#熱銷 #裝飾畫 #壁畫北歐 #裝飾 #北歐裝飾畫 #壁畫 #掛畫 #客製化 #餐廳掛畫 #生日禮物 #黑白色系 #壁畫 #客廳掛畫 #ins #北歐 #居家裝飾 #無框畫 #牆壁裝飾"
);
keyWordMap.set(
  "Lily-收納盒",
  "#生活 #收納 #收納籃 #桌面收納盒 #收納盒桌上收納 #整理盒 #抽屜收納盒 #桌上收納盒 #儲物盒 #抽屜式收納盒 #化妝收納盒 #分隔收納盒 #保養品收納盒 #收納盒置物盒 #收納筐 #雜物"
);
keyWordMap.set(
  "Lily-沙發墊",
  "#沙發罩 #沙發套 #沙發巾 #超彈沙發套 #沙發坐墊 #防滑沙發墊 #四季通用 #防水沙發墊 #寵物沙發墊 #隔尿沙發墊 #防水 #隔尿 #純色 #水晶絨 #保暖 #北歐 #居家裝飾"
);
keyWordMap.set(
  "Lily-窗簾",
  "#窗簾 #成品窗簾 #多款式 #多尺 #棉麻 #簡约 #現代 #素色窗簾 #遮陽簾 #韓式 #落地窗簾 #短窗簾 #掛鉤窗簾 #伸縮桿 #門簾布簾 #隔熱防曬布簾 #臥室窗簾成品"
);
keyWordMap.set(
  "Lily-地墊",
  "#保暖地墊 #軟墊舒適 #法蘭絨毯 #地墊地毯 #客廳地墊 #北歐地墊 #廚房踏墊 #門口踏墊 #爬行墊 #居家擺飾 #腳踏墊 #地墊 #地毯 #北歐風 #床邊地墊 #居家佈置 #長地毯 #居家裝飾"
);
keyWordMap.set(
  "Lily-椅套",
  "#椅套 #椅子套 #椅墊 #辦公椅套 #座椅套 #純色椅套 #彈力椅套 #餐椅套 #酒店椅套 #椅墊 #簡约椅套 #棉麻椅套 #格紋椅套 #沙發椅套 #家居裝飾 #保護套 #椅罩 #生活"
);
keyWordMap.set(
  "Lily-門簾",
  "#客製化 #門簾 #長門簾 #加長門簾 #穿桿式門簾 #免打孔門簾 #裝飾 #門簾 #玄關 #客廳 #可愛門簾 #門簾遮簾 #風水簾 #北歐風 #一片門簾 #風水簾一片式 #居家 #生活"
);
keyWordMap.set(
  "Lily-相框",
  "#北歐 #北歐相框 #客製化 #相框 #拼圖框 #畫框 #裱框 #藝術品 #木質框 #框 #木相框 #北歐風格 #作品框 #裝飾 #婚禮相框 #結婚相框 #居家 #生活"
);
keyWordMap.set(
  "Lily-壁紙",
  "#北歐壁貼 #素色壁貼 #素色壁紙 #純色壁紙 #壁貼 #客廳 #防水 #自粘壁貼 #白色壁紙 #自黏壁紙 #壁紙壁貼窗貼 #牆紙 #壁貼窗貼 #壁紙 #牆貼 #墻壁貼紙 #居家 #生活"
);
keyWordMap.set(
  "Lily-浴簾",
  "#现货 #浴室 #北歐風 #防水防霉 #浴簾 #淋浴簾 #加厚 #免打孔 #隔間簾 #隔斷簾 #掛簾 #客製尺寸 #附掛環 #防水浴簾 #浴室窗簾 #浴室浴簾 #浴室免打孔 #居家 #生活"
);
keyWordMap.set(
  "Lily-床上用品",
  "#床單 #被套 #單人床包 #雙人床包 #加大床包 #特大床包 #兩用被套 #涼被 #现货 #四件套 #北歐風 #ins風 #純棉床單 #被單被子被套 #輕奢 #居家 #生活"
);
keyWordMap.set(
  "Lily-桌墊",
  "#桌布 #餐桌墊 #桌墊 #餐桌墊 #北歐風 #防水防油 #電腦桌墊 #加厚 #大號桌墊 #PVC桌墊 #桌巾防水防油 #桌布防水防油 #餐桌布防水 #防水 #防油 #居家 #生活"
);
keyWordMap.set(
  "Lily-挂鈎",
  "#掛鉤 #壁掛 #衣帽架 #餐桌墊 #北歐風 #ins風 #鑰匙掛勾 #裝飾 #鑰匙架 #衣服掛鉤 #衣帽掛鉤 #掛鉤北歐 #餐桌布防水 #衣帽掛勾 #北歐風衣帽架 #居家 #生活"
);
keyWordMap.set(
  "Lily-家具贴纸",
  "#家具贴 #贴纸 #桌子贴纸 #衣柜贴纸 #北歐風 #ins風 #化妆台贴纸 #厨房贴纸 #纯色贴纸 #桌貼 #衣櫥貼 #電腦桌貼 #衣櫃貼紙 #家具裝飾 #居家 #生活"
);
keyWordMap.set(
  "Lily-墙壁装饰",
  "#墙壁装饰 #墙贴 #可爱 #墙壁美化 #北歐風 #ins風 #光涂层 #耐用墙壁装饰 #耐看墙壁装饰 #耐用 #耐看 #实用 #实用墙贴 #裝飾 #居家 #生活"
);
keyWordMap.set(
  "Lily-其他",
  "#北歐風 #ins風 #可爱 #裝飾 #居家 #生活 #耐用 #耐看"
);

