var showBox, skuBox, input;
var index = 0,
  skuLength, originalPrice, firstSku, longestSkuLength = 0;
// SKU过滤规则
var skuFilter = ['尺寸', '尺码', '大小'];
var pddData = {
  pddKeyWord: '沙發墊',
  initialFreight: 9,
  addHeavyFreight: 2
};
var goods = [];
// 商品标题前缀
const titlePrefix = '免運極速發貨廠家直銷';
// 商品标题后缀
const titleSuffix = '热销爆款现货北欧简约';

document.addEventListener('DOMContentLoaded', async () => {
  await wait(1000);
  // 初始化
  await init();

  // 获取并设置商品SKU
  await getSku(index);

  // 获取并设置商品详情
  await getDetail();
})

// isInclude柯理化
function skuFilterFn(val) {
  return isInclude(val, skuFilter);
}

// 延时器
function wait(timeout) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, timeout);
  })
}
// 初始化
async function init() {
  // 出入一个box
  showBox = document.createElement('div');
  showBox.setAttribute("class", "showBox");
  document.body.appendChild(showBox);
  skuLength = $('.sku-spec-value-list').length;

  $('.showBox').append(
    '<div>' +
    '<button class="leftBtn pddBtn">切换SKU</button>' +
    '<select id="keyWordSelect">' +
    '<option value="Lily-沙發墊">沙發墊</option>' +
    '<option value="Lily-窗簾">窗簾</option>' +
    '<option value="Lily-地墊">地墊</option>' +
    '<option value="Lily-桌墊">桌墊</option>' +
    '<option value="Lily-床上用品">床上用品</option>' +
    '<option value="Lily-浴簾">浴簾</option>' +
    '<option value="Lily-壁紙">壁紙</option>' +
    '<option value="Lily-掛畫">掛畫</option>' +
    '<option value="Lily-門簾">門簾</option>' +
    '<option value="Lily-椅套">椅套</option>' +
    '</select><br>' +
    '第一个SKU：<span class="firstSku"></span>' +
    '<button class="copyFirstSkuBtn pddBtn" style="margin-left:20px;">拷</button><br>' +
    '</div>' +
    '<div>' +
    '<form action="" method="get"> ' +
    '您最喜欢水果？<br /><br /> ' +
    '<label><input name="Fruit" type="radio" value="" />苹果 </label>  ' +
    '<label><input name="Fruit" type="radio" value="" />桃子 </label>  ' +
    '<label><input name="Fruit" type="radio" value="" />香蕉 </label>  ' +
    '<label><input name="Fruit" type="radio" value="" />梨 </label>  ' +
    '<label><input name="Fruit" type="radio" value="" />其它 </label>  ' +
    '</form> ' +
    '<div class="priceBox">' +
    '<button id="oneKeyPackaging" class="pddBtn" style="display=block;width:100%;">一键打包</button>' +
    '原价：<input id="input" class="originPrice">CYN<br>' +
    '转换后价格为：NT$<span class="targetPrice"></span>' +
    '<button class="copyPriceBtn pddBtn" style="margin-left:20px;">拷</button><br>' +
    '初始运费：<input id="initialFreight" value="19">CYN<br>' +
    'SKU递增运费：<input id="addHeavyFreight" value="7">CYN<br>' +
    '</div><hr />' +
    // 下面为拷贝信息+
    '<div id="descBox" style="margin-top:20px;">' +
    '<span class="otherSizeSpan">Hi ~ 歡迎光臨Dan`S iDeA < /span>' +
    '<span class="otherSizeSpan">賣場主要提供多姿多彩的創意生活品~~</span>' +
    '<span class="otherSizeSpan">各位人客喜歡的可以下標唷~~</span>' +
    '<br><span class="otherSizeSpan">⚠️各位大大水水們，本館所有訂單滿💰199元才發貨唷</span>' +
    '<br><span class="otherSizeSpan">⚠️各位大大水水們，本館所有訂單滿💰199元才發貨唷</span>' +
    '<br><span class="otherSizeSpan">⚠️各位大大水水們，本館所有訂單滿💰199元才發貨唷</span>' +
    '<br><br><span class="otherSizeSpan">選擇我們的理由：</span>' +
    '<span class="otherSizeSpan">1、溫馨服務：想客戶所想，為客戶所為！大功成於細節，服務力求完滿！</span>' +
    '<span class="otherSizeSpan">2、質量保證：可能別家的價格比本賣場低，但是質量是沒我們好的！寶貝質量有保障，性價比超讚，歡迎訂購哟</span>' +
    '<br><br><span class="otherSizeSpan">【其他組合下標指南】:如價格為290，下標先選【組合訂購款】選項，這個選項單價為1元，數量欄填入290即可！（備注寫需要的款示即可）</span>' +
    '<br><div class="detailBox"></div>' +
    '<br><span class="otherSizeSpan">【商品尺寸、价格】</span>' +
    '<div class="skuBox"></div>' +
    '<br><span class="otherSizeSpan">温馨提示：</span>' +
    '<span class="otherSizeSpan">关于色差</span>' +
    '<span class="otherSizeSpan">因拍攝燈光，環境和顯示器等不同的外界因素，會產生輕微色差具體以收到的實物為準！</span>' +
    '<br><br><span class="otherSizeSpan">【注意】</span>' +
    '<span class="otherSizeSpan">7-11和超商取貨範圍在（材積：需≦45cm*30cm*30cm，最長邊≦45cm，其他兩邊則需均≦30cm；重量不得超過5公斤）為限。</span>' +
    ' <span class="otherSizeSpan">故訂購數量較多件的或者商品較大的。建議選擇宅配，宅配」物流方式，以避免因超材超重無法配送而耽誤收貨時間。</span>' +
    '<br><span class="otherSizeSpan keyWordSpan">#保暖地墊 #軟墊舒適 #法蘭絨毯 #地墊地毯 #客廳地墊 #北歐地墊 #北歐地墊#廚房踏墊 #門口踏墊 #爬行墊 #居家擺飾 #腳踏墊 #地墊 #地毯  #北歐風</span>' +
    '<div>'
  )

  // 上方填了了DOM才可
  skuBox = document.querySelector('skuBox');

  await wait(1000);

  // 选择相对应的关键字
  $('#keyWordSelect').change((e) => {
    var keyWord = $('#keyWordSelect').children('option:selected').val();
    switch (keyWord) {
      case 'Lily-沙發墊': {
        $('.keyWordSpan').text('#沙發罩 #沙發套 #沙發巾 #超彈沙發套 #沙發坐墊 #防滑沙發墊 #四季通用 #防水沙發墊 #寵物沙發墊 #隔尿沙發墊 #防水 #隔尿 #純色 #水晶絨 #保暖 #北歐 #居家裝飾');
        pddData.pddKeyWord = 'Lily-沙發墊';
        localStorageSetItem('pddData', pddData);
        break;
      }
      case 'Lily-窗簾': {
        $('.keyWordSpan').text('#現貨#貨到付款#窗簾#成品窗簾#多款式#多尺#棉麻#簡約#現代#素色窗簾#遮陽簾#韓式#落地窗簾#短窗簾#掛鉤窗簾#伸縮桿#門簾布簾#隔熱防曬布簾#臥室窗簾成品 ');
        pddData.pddKeyWord = 'Lily-窗簾';
        localStorageSetItem('pddData', pddData);
        break;
      }
      case 'Lily-地墊': {
        $('.keyWordSpan').text('#保暖地墊 #軟墊舒適# 法蘭絨毯# 地墊地毯# 客廳地墊# 北歐地墊# 北歐地墊# 廚房踏墊# 門口踏墊# 爬行墊# 居家擺飾# 腳踏墊# 地墊# 地毯# 北歐風# 床邊地墊# 居家佈置# 長地毯# 居家裝飾 ');
        pddData.pddKeyWord = 'Lily-地墊';
        localStorageSetItem('pddData', pddData);
        break;
      }
      case 'Lily-桌墊': {
        $('.keyWordSpan').text('#桌墊 #客製化 #客製化桌墊 #餐桌墊 #北歐風 #防水防油 #電腦桌墊 #加厚 #大號桌墊 #PVC桌墊');
        pddData.pddKeyWord = 'Lily-桌墊';
        localStorageSetItem('pddData', pddData);
        break;
      }
      case 'Lily-床上用品': {
        $('.keyWordSpan').text('#單人床包 #雙人床包 #加大床包 #特大床包  #兩用被套 #涼被　#現貨');
        pddData.pddKeyWord = 'Lily-床上用品';
        localStorageSetItem('pddData', pddData);
        break;
      }
      case 'Lily-浴簾': {
        $('.keyWordSpan').text('#現貨 #貨到付款 #北歐風 #防水防霉 #浴簾 #淋浴簾 #加厚 #免打孔 #隔間簾 #隔斷簾 #掛簾 #客製尺寸 #附掛環');
        pddData.pddKeyWord = 'Lily-浴簾';
        localStorageSetItem('pddData', pddData);
        break;
      }
      case 'Lily-壁紙': {
        $('.keyWordSpan').text('#北歐壁貼# 素色壁貼# 素色壁紙# 純色壁紙# 壁貼# 客廳# 臥室# 防水# 自粘壁貼# 白色壁紙# 自黏壁紙# 壁紙壁貼窗貼# 牆紙# 壁貼窗貼# 壁紙# 牆貼# 墻壁貼紙');
        pddData.pddKeyWord = 'Lily-壁紙';
        localStorageSetItem('pddData', pddData);
        break;
      }
      case 'Lily-相框': {
        $('.keyWordSpan').text('#相框 #拼圖框 #畫框 #裱框 #藝術品 #木質框 #框 #木相框# 北歐風格# 作品框 ');
        pddData.pddKeyWord = 'Lily-相框';
        localStorageSetItem('pddData', pddData);
        break;
      }
      case 'Lily-掛畫': {
        $('.keyWordSpan').text('#客製化 #餐廳掛畫 #生日禮物 #黑白色系 #年輪 #油畫 #裝飾畫 #掛畫 #壁貼 #壁畫 #客廳掛畫 #ins #北歐 #居家裝飾 #民宿 #無框畫');
        pddData.pddKeyWord = 'Lily-掛畫';
        localStorageSetItem('pddData', pddData);
        break;
      }
      case 'Lily-門簾': {
        $('.keyWordSpan').text('#客製化 #門簾 #裝飾 #門簾 #玄關 #客廳 #可愛門簾 #小銅板 #風水簾 #北歐風');
        pddData.pddKeyWord = 'Lily-門簾';
        localStorageSetItem('pddData', pddData);
        break;
      }
      case 'Lily-椅套': {
        $('.keyWordSpan').text('#椅套 #椅子套 #椅套 #辦公椅套 #座椅套 #純色椅套 #彈力椅套 #餐椅套 #酒店椅套 #椅墊 #簡約椅套 #棉麻椅套 #格紋椅套 #沙發椅套 #家居裝飾');
        pddData.pddKeyWord = 'Lily-椅套';
        localStorageSetItem('pddData', pddData);
        break;
      }
      default:
        break;
    }
  })

  // 左切换SKU
  $('.leftBtn').click(() => {
    if (index == 0) {
      index = skuLength - 1;
    } else {
      index--;
    }
    getSku(index);
  })

  $('.copyPriceBtn').click(() => {
    $copyText(originalPrice);
  })

  $('.copyFirstSkuBtn').click(() => {
    $copyText(firstSku);
  })

  // 监听价格变化
  $("#input").change(() => {
    getPrice();
  })

  // 监听初始运费
  $("#initialFreight").change(() => {
    pddData.initialFreight = $('#initialFreight').val();
    localStorageSetItem('pddData', pddData);
    getPrice();
    renderDom(goods);
  })

  // 监听SKU递增运费
  $("#addHeavyFreight").change(() => {
    pddData.addHeavyFreight = $('#addHeavyFreight').val();
    localStorageSetItem('pddData', pddData);
    getPrice();
    renderDom(goods);
  })

  // 一键打包
  $("#oneKeyPackaging").click((e) => {
    let data = {
      /* 第一个SKU，用于设置最小尺寸 */
      firstSku,
      /* 第一个SKU对应的价格，用于统一价格 */
      originalPrice,
      /* 商品标题 */
      goodsTitle: titlePrefix + $('.enable-select').text() + titleSuffix,
      /* 设置商品分类 */
      firstClassify: $('#keyWordSelect').children('option:selected').val()
    }

    /* 商品规格数量 */
    data.skuKindNum = skuLength;
    /* 需要过滤的商品规格，有2个才设置，会将其对应的初始化 */
    data.filterSkuKind = '';

    if (skuLength > 1) {
      data.filterSkuKind = $('.r-mksVqr:eq(' + index + ')').children('span').text();
    }

    /* 需要复制的内容的 */
    const range = document.createRange();
    range.selectNode(document.getElementById('descBox'));
    const selection = window.getSelection();
    if (selection.rangeCount > 0) selection.removeAllRanges();
    selection.addRange(range);
    data.goodsDesc = window.getSelection().toString();

    let str = JSON.stringify(data);
    $copyText(str);
  })

  // 设置默认值SKU
  if (localStorageGetItem('pddData')) {
    pddData = localStorageGetItem('pddData');
    // 读取存在local的关键字
    if (pddData.pddKeyWord) {
      $('#keyWordSelect').val(pddData.pddKeyWord);
      // 触发事件
      $('#keyWordSelect').change();
    }

    if (pddData.initialFreight) {
      $('#initialFreight').val(pddData.initialFreight);
    }

    if (pddData.addHeavyFreight) {
      $('#addHeavyFreight').val(pddData.addHeavyFreight);
    }
  }

  Array.from($('.r-mksVqr .sku-specs-key')).forEach((e, index1) => {
    if (skuFilterFn(e.textContent)) {
      index = index1;
    }
  })
}

// 算法计算公式
function sum(val, i = 0) {
  let initialFreight = 19,
    addHeavyFreight = 7;

  initialFreight = parseFloat($('#initialFreight').val());
  addHeavyFreight = parseFloat($('#addHeavyFreight').val());

  return Math.ceil(((parseFloat(val) + initialFreight + addHeavyFreight * i) * 1.54 * 4.5) * 4);
}

// 根据input换算出价格
function getPrice() {
  let val = $('#input').val();
  originalPrice = sum(val);
  $('.targetPrice').text(originalPrice);
}

// 获取商品详情
function getDetail() {
  var detail;
  for (let i = 0; i < $('._8rUS_gSm').length; i++) {
    detail = $('._8rUS_gSm ._1M3pVo3W:eq(' + i + ')').text() + ":" +
      $('._8rUS_gSm ._32_tX1hK:eq(' + i + ')').text();

    if (detail.includes('品牌'))
      continue;

    var br = document.createElement('br');
    var textNode = document.createTextNode(detail);
    $(".detailBox").append(textNode);
    $(".detailBox").append(br);
  }
}

// 获取当前商品的SKU，如果是样式加尺寸则获取SKU
function getSku(index) {
  goods = [];

  // 判断是否有库存，没有则下一个
  function selectFirstItem(btn) {
    if (btn[0].outerHTML.includes('sku-spec-value-disable')) {
      selectFirstItem(btn.next())
    } else {
      btn.click();
    }
  }
  // 所有的SKU选中第一个
  for (var i = 0; i < skuLength; i++) {
    selectFirstItem($('.sku-spec-value-list:eq(' + i + ')').children('div:first-child'));
  }

  // 主SKU
  var mainSku = $('.sku-spec-value-list:eq(' + index + ')');

  // 遍历SKU
  Array.from(mainSku.children()).forEach((element, index1) => {
    if (index1 != 0)
      element.click();
    let val = $('._9AczZsFY ._27FaiT3N').text();
    goods.push({
      sku: element.textContent,
      originPrice: val.slice(1),
    })
    if (strlen(goods[index1].sku) > longestSkuLength) {
      longestSkuLength = strlen(goods[index1].sku);
    }
  });

  // 价格排序
  goods.sort((a, b) => {
    return a.originPrice - b.originPrice;
  })

  // 去除定制SKU
  goods = goods.filter(e => {
    let flag = !e.sku.includes('定制') && !e.sku.includes('尺寸') && !e.sku.includes('客服') && !e.sku.includes('备注') && !e.sku.includes('定做');
    return flag;
  })

  renderDom(goods);
}

// 渲染DOM
function renderDom(goods) {
  // 清空SKU显示列表
  $(".skuBox").empty();

  // 梯度增加价格，避免快递费问题
  goods.forEach((element, index1) => {
    element.targetPrice = Math.ceil(sum(element.originPrice, index1) / 4);
  });

  // 渲染在DOM文档对象模型中
  goods.forEach((e, index) => {
    let addSpaceNum = longestSkuLength - strlen(e.sku);
    let str = '';
    for (var i = 0; i < addSpaceNum; i++) {
      str = str + '\u00A0';
    }
    $(".skuBox").append(`<span>${e.sku} \u00A0\u00A0 ${str} ${e.targetPrice}元</span><br>`);
  })

  // 获取价格
  $('#input').val(goods[0].originPrice);
  getPrice();

  // 设置第一个SKU
  $('.firstSku').text(goods[0].sku);
  firstSku = goods[0].sku;
}