/*
 * @Author: Penk
 * @LastEditors: Penk
 * @LastEditTime: 2021-05-24 23:48:32
 * @FilePath: \多多插件-妹\js\content_scripts_kyy.js
 */
// 是否自动初始化
var autoPlay = false;
// SKU过滤规则
var skuFilter = ['尺寸', '尺码', '大小'];
var skuKindNum = 0;
// 商品标题前缀
var goodsPrefix = '免運極速發貨廠家直銷';
// 商品标题后缀
var goodsSuffix = '热销爆款现货北欧简约';
// 库存
var stock = '1000';
// 商品重量
var spzl = 1;
// 商品尺寸
var spcc = [1, 1, 1];
// 物流方式
var wlfs = ['7-11',
  '全家',
  '萊爾富',
  'OK Mart',
  '賣家宅配：大型/超重物品運送'
];
var timestamp = new Date().getTime();

// 通用SKU
var generalSku = '🌈其他尺寸請看描述價格下單唷';
// 初始价
var originalPrice = '1872';
// 第一个SKU
var firstSku = '30*40';
// 商品标题
var goodsTitle = '~~~~';
// 商品描述
var goodsDesc = '~~~~';
// 第一个分类描述
var firstClassify = '';
// 初始化的分类描述
var filterSkuKind = '';

// 是否在运行中
var isRunning = false;

window.addEventListener('load', async () => {
  // 右上角按钮框
  initBox();
})

//监听刷新页面事件方法
window.onbeforeunload = function (event) {
  console.log('onbeforeunload');
};

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  // console.log(sender.tab ?"from a content script:" + sender.tab.url :"from the extension");
  if (request.cmd == 'initCollect') {
    initCollect();
    sendResponse('我收到了你的消息！');
  }
});

// 遍历锚点，处理懒加载
function anchors(target, timeout = 600) {
  if (target) {
    for (var i = 0; i < $('.anchors span').length; i++) {
      if ($('.anchors span').eq(i).text() == target) {
        $('.anchors span').eq(i).trigger('click');
        break;
      }
    }
    return;
  }
  for (var i = 0; i < $('.anchors span').length; i++) {
    !(function (i) {
      setTimeout(() => {
        $('.anchors span').eq(i).trigger('click');
      }, timeout * i);
    })(i)
  }
  setTimeout(() => {
    $('.anchors span').eq(0).trigger('click');
  }, timeout * ($('.anchors span').length));
}

async function initCollect() {
  anchors('店铺类目');
  // 选择適用空間
  await selectItem('臥室(卧室)');

  // 选择風格
  await selectItem('歐式(欧式)');

  // 选择材质
  await selectItem('混紡(混纺)');


  // 选择品牌
  await selectItem('自有品牌(自有品牌)');
  anchors('商品信息');

  // 选择客优云分类
  await selectItem(firstClassify);

  // 修改商品title
  await addGoodsSuffix();
  // 添加商品编码
  await updateInput($('.app div[title="商品编码"]'), timestamp);
  // 添加商品描述
  await addGoodsDesc();

  anchors('价格库存');

  // 需要过滤才初始化SKU以及设置价格表
  if (filterSkuKind != '') {

    // 初始化SKU
    await initSku();

    anchors('定价工具');
    // 批量编辑
    await wait(1000);
    await bulkEdit();
  }

  anchors('物流信息');
  // 物流信息
  expressInfo();

}

function selectTW(timeout = 2000) {
  // 台湾站点按钮 
  return new Promise((resolve, reject) => {
    var twBtn = document.querySelectorAll('#main .v-content__wrap .v-tabs__item')[1];
    twBtn.click();
    setTimeout(async () => {
      // 选择店铺
      await selectItem('mjvrvuiadn');

      resolve();
    }, timeout);
  })
}

// 下拉框选择按钮
function selectItem(itemName, timeout = 500) {
  return new Promise((resolve, reject) => {
    Array.from($('.app .v-list .v-list__tile__title')).forEach(item => {
      if (item.textContent == itemName) {
        item.click();
      }
    })
    setTimeout(() => {
      resolve();
    }, timeout);
  })
}

// 修改商品title
function addGoodsSuffix() {
  return new Promise(async (resolve, reject) => {
    // 找到句柄
    let handle = $('.app div[title="商品名称"]').next();

    handle.find('.k-sensitive-words-input').click(async (e) => {
      await wait(500);

      // 清除事件
      // 点击后出来input才能执行，需要放在timeout中
      await inputValue(handle.find('input')[0], goodsTitle);
      handle.find('.k-sensitive-words-input').unbind(e);
      resolve();
    })

    // 手动触发
    handle.find('.k-sensitive-words-input__mask')[0].click();
  })
}

// 修改input标签
function updateInput(handle, val) {
  return new Promise((resolve, reject) => {
    // 找到句柄
    handle = handle.next();

    handle.find('.v-input__control').click(async (e) => {
      await wait(500);

      // 点击后出来input才能执行，需要放在timeout中
      var inputHandle = handle.find('textarea').length > 0 ? handle.find('textarea')[0] : handle.find('input')[0];
      await inputValue(inputHandle, val);
      // 清除事件
      handle.find('.v-input__control').unbind(e);
      resolve();
    })

    // 手动触发
    handle.find('.v-input__control')[0].click();
  })
}

// 添加商品描述
function addGoodsDesc() {
  return new Promise(async (resolve, reject) => {
    // 找到句柄
    let handle = $('.app div[title="描述"]').next();

    handle.find('.k-sensitive-words-input').click(async (e) => {
      await wait(500);

      // 点击后出来input才能执行，需要放在timeout中
      await inputValue(handle.find('textarea')[0], goodsDesc);
      // 清除事件
      handle.find('.k-sensitive-words-input').unbind(e);
      resolve();
    })

    // 手动触发
    handle.find('.k-sensitive-words-input__mask')[0].click();
  })
}

// 初始化SKU
async function initSku(timeout = 1000) {
  return new Promise(async (resolve, reject) => {

    var handle = $('.variation-option-creator');
    let arr = Array.from(handle);

    // 第一个SKU不能超过50个
    for (var ai = 0; ai < arr.length; ai++) {
      let temp = arr[ai].getElementsByTagName('input');
      console.log(temp[0].value, filterSkuKind);
      if (arr[ai].getElementsByTagName('input')[0].value != filterSkuKind) {
        let more = $('.variation-option-creator:eq(' + ai + ')').find('.v-input__append-outer i').length - 25;
        for (var i = 0; i < more; i++) {
          await deleteSku($('.variation-option-creator:eq(' + ai + ')').find('.v-input__append-outer i').last());
        }
      }
    }
    for (var ai = 0; ai < arr.length; ai++) {
      if (arr[ai].getElementsByTagName('input')[0].value == filterSkuKind) {
        while ($('.variation-option-creator:eq(' + ai + ')').find('.v-input__append-outer i')[0]) {
          await deleteSku($('.variation-option-creator:eq(' + ai + ')').find('.v-input__append-outer i').eq(0));
        }

        // 添加第一个有用SKU
        $('.variation-option-creator:eq(' + ai + ')').find('button').eq(0).click();
        await addSku(ai, firstSku);

        // 添加通用SKU
        $('.variation-option-creator:eq(' + ai + ')').find('button').eq(0).click();
        await addSku(ai, generalSku);
      }
    }
    await wait(timeout);
    resolve();
  })
}

// 删除默认SKU
async function deleteSku(deleteBtn, timeout = 100) {
  return new Promise(async (resolve, reject) => {
    await wait(timeout);
    deleteBtn.trigger("click");
    resolve();
  })
}

// 添加新的SKU
function addSku(skuIndex, skuName) {
  return new Promise((resolve, reject) => {
    setTimeout(async () => {
      let skuHandle = $('.variation-option-creator:eq(' + skuIndex + ')').find('input').last()[0];
      await inputValue(skuHandle, skuName);
      resolve();
    }, 1000);
  })
}

// 批量编辑
function bulkEdit(timeout = 1000) {
  return new Promise(async (resolve, reject) => {
    // 价格计算方式
    selectItem('统一值');

    // 小数处理方式
    selectItem('取整');

    // 规格编码
    await inputValue($('input[aria-label="规格编码"]')[0], timestamp);

    // 价格
    var originalPriceInput = $('div[title="批量编辑"]').parent().find('input[aria-label="价格"]')[0];
    await inputValue(originalPriceInput, originalPrice);

    // 库存
    var stockInput = $('div[title="批量编辑"]').parent().find('input[aria-label="库存"]')[0];
    await inputValue(stockInput, stock);

    await wait(1000);

    // 一键设置按钮
    var bulkBtn = $('div[title="批量编辑"]').parent().find('.v-btn__content')[0];
    bulkBtn.click();
    resolve();

    await wait(2000);
    await setOriginPrice();
  })
}

// 通用价格设置价格
async function setOriginPrice() {
  let temp = getHandleBySelectorAndText($(".v-table td"), generalSku);
  temp = $(temp).next().find('.k-text-field__input')[0];
  await inputValue(temp, Math.ceil(originalPrice / 5));
}

// 设置价格
async function setTargetPrice() {
  let temp = parseInt($('#variationTableRow1 .k-text-field-content-value__text').text());

  let value = Math.ceil(temp / 5 / 100) > 10 ? Math.ceil(temp / 5 / 100) : 10;

  let node = $('div[title="商品规格列表"]').next().find('.k-text-field__input');
  await inputValue(node[5], value);
}

// 物流信息
async function expressInfo() {
  // 商品重量
  var spzlInput = $('#logisticsInfo div[title=商品重量]').next().find('input')[0];
  inputValue(spzlInput, spzl);

  // 商品尺寸
  var spccInput = $('#logisticsInfo div[title=商品尺寸]').next().find('input');
  inputValue(spccInput[0], spcc[0]);
  inputValue(spccInput[1], spcc[1]);
  inputValue(spccInput[2], spcc[2]);

  for (let item of wlfs) {

    let handle = $(`#logisticsInfo input[aria-label="${item}"]`);
    if (handle.length > 0) {
      await checkBox(handle[0], handle[0].getAttribute('aria-checked'), false);
      handle.parent().parent().parent().parent().next('i').click();
    }

  }
  await wait(500);
  console.log($('input[aria-label="运费设置"]')[0])
  // 添加商品编码
  inputValue($('input[aria-label="运费设置"]')[0], 90);
}

// 设置input值
function inputValue(el, val) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      let evt = document.createEvent('HTMLEvents');
      evt.initEvent('input', true, true);
      el.value = val;
      el.dispatchEvent(evt);
      resolve();
    }, 1000);
  })
}

// 设置checkBox
function checkBox(el, attr, val) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (attr == (val + '')) {
        el.click();
      }
      resolve();
    }, 50);
  })
}

// 客优云弄个固定窗口存放按钮
async function initBox() {
  var kyyBox = document.createElement('div');
  kyyBox.setAttribute("class", "kyyBox");
  document.body.appendChild(kyyBox);

  $('.kyyBox').append(
    '<div class="kkyContent">' +
    '<textarea rows="5" class="textarea">请替换拼多多一键打包的数据，采集页面才需要~~~</textarea>' +
    '<br>' +
    '<button class="collectBtn pddBtn">一键采集默认折扣</button>' +
    '<br>' +
    '<button class="goodsBtn pddBtn">一键折扣</button>' +
    '<br>' +
    '<button class="initBtn pddBtn">一键初始化</button>' +
    '<br>' +
    // '<button class="testBtn pddBtn">测试</button>' +
    '</div>'
  )

  await wait(500);
  $(".textarea").bind('input propertychange', () => {
    let data = JSON.parse($(".textarea").eq(0).val());
    firstSku = data.firstSku;
    originalPrice = data.originalPrice;
    goodsDesc = data.goodsDesc;
    goodsTitle = data.goodsTitle;
    skuKindNum = data.skuKindNum;
    firstClassify = data.firstClassify;
    filterSkuKind = data.filterSkuKind;
    skuFilter.push(data.filterSkuKind);


    // 避免复制上上次的数据
    $copyText('请先拷贝拼多多一键打包数据，粘贴一次即可~~~');
  })


  // 一键采集按钮
  $('.collectBtn').click(async () => {
    if ($(".textarea").eq(0).val() == '请替换拼多多一键打包的数据，采集页面才需要~~~') {
      console.log('请先粘贴拼多多数据...');
      return;
    }


    if (isRunning) {
      console.log('isRunning...');
      return;
    }

    isRunning = true;

    await wait(1000);

    // 选择台湾站点
    await selectTW();

    // 遍历锚点，处理懒加载
    anchors();

    setTimeout(async () => {
      await initCollect();

      if (filterSkuKind != '') {
        await oneKeyDiscount();
      }
      isRunning = false;
    }, 8000);
  })



  // 一键折扣
  $('.goodsBtn').click(async () => {
    if (isRunning) {
      console.log('isRunning...');
      return;
    }

    isRunning = true;

    await oneKeyDiscount();

    // 自动更新
    getHandleBySelectorAndText($('.v-btn__content'), '更新').click();

    isRunning = false;
  })

  $('.initBtn').click(async () => {
    if (isRunning) {
      console.log('isRunning...');
      return;
    }

    isRunning = true;

    initKyy();

    // 自动更新

    isRunning = false;
  })
}

async function initKyy() {
  anchors('店铺类目');
  // 选择適用空間
  await selectItem('臥室(卧室)');

  // 选择風格
  await selectItem('歐式(欧式)');

  // 选择材质
  await selectItem('混紡(混纺)');

  // 选择品牌
  await selectItem('自有品牌(自有品牌)');

  // 添加商品编码
  await updateInput($('.app div[title="商品编码"]'), timestamp);
  // 规格编码
  await inputValue($('input[aria-label="规格编码"]')[0], timestamp);

  anchors('物流信息');
  // 物流信息
  expressInfo();

}

async function oneKeyDiscount() {
  await wait(1000);

  // 跳转
  anchors('定价工具');
  await wait(1000);

  // 设置最低价格
  // await setTargetPrice();
  // await wait(1200);

  // 翻译
  $('.bottom-panel button').eq(0).click();
  getHandleBySelectorAndText($('.v-list__tile__title'), '中文(简)->繁').trigger('click');
  await wait(500);

  // 点击折扣活动
  $('input[aria-label="折扣活动"]').click();
  await wait(500);

  // 选择折扣
  $('#inspire input[role="radio"]').eq(0).click();
  await wait(200);

  // 点击确定按钮
  $('#inspire .v-btn__content')[3].click();

  // 设置折扣值
  await inputValue($('input[aria-label="折扣"]')[1], 75);
  await wait(200);

  // 打开折扣按钮
  $('div[title="商品规格列表"]').next().find('th').eq(3).find('input').click();

  await wait(1500);
  // 点击一键设置
  $('div[title="折扣设置"]').parent().find('.v-btn__content').eq(0).click();
  await wait(1200);
}

function getHandleBySelectorAndText(selector, text) {
  let handle = typeof selector == 'String' ? $(selector) : selector;
  let len = handle.length;
  let temp;
  for (var i = 0; i < len; i++) {
    temp = handle.eq(i).text();
    if (temp.includes(text)) {
      return handle.eq(i);
    }
  }
}